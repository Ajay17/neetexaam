-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 28, 2018 at 08:52 PM
-- Server version: 5.7.23-0ubuntu0.16.04.1
-- PHP Version: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `neetdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(10) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(1, 'admin', '$2a$10$xaeo5XnOGr/uh9vSOXv/..y0pFje/i3EoHeDKnjrDhy42rY.KTLVu');

-- --------------------------------------------------------

--
-- Table structure for table `exam_time`
--

CREATE TABLE `exam_time` (
  `id` int(10) NOT NULL,
  `time` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_time`
--

INSERT INTO `exam_time` (`id`, `time`) VALUES
(1, '2018-08-14 03:55');

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `id` int(10) NOT NULL,
  `faq_ques` varchar(255) NOT NULL,
  `faq_ans` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`id`, `faq_ques`, `faq_ans`) VALUES
(1, 'hi how are you?', 'hanji'),
(8, 'hlo sir ji', 'how are you ? ok');

-- --------------------------------------------------------

--
-- Table structure for table `institute`
--

CREATE TABLE `institute` (
  `id` int(10) NOT NULL,
  `contact_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `institute_name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `institute`
--

INSERT INTO `institute` (`id`, `contact_name`, `email`, `mobile`, `password`, `address`, `city`, `website`, `institute_name`, `phone`) VALUES
(37, 'dsdsa', 'sad@fgfs.gd', '4444444444', '4', '4', '4', '4', '4', '4'),
(38, 'sda', 'dasd@fsdgf.dfgd', '2134141242', '2', '2', '2', '2', '2', '22'),
(39, 'xzcxzc', 'cxzc@fsdf.df', '3343432423', '3', '3', '3', '3', '3', '434'),
(40, 'dsadsa', 'dsf@fsd.fd', '2353532532', '3', '3', '3', '3', '3', '3'),
(41, 'test', 'test@test.test', '1111111111', 'test', 'test', 'test', 'test', 'test', '111111111');

-- --------------------------------------------------------

--
-- Table structure for table `page_price`
--

CREATE TABLE `page_price` (
  `id` int(10) NOT NULL,
  `exam_name` varchar(255) NOT NULL,
  `exam_type` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `validity` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE `question` (
  `id` int(10) NOT NULL,
  `question_set_id` int(10) NOT NULL,
  `question` varchar(255) NOT NULL,
  `option1` varchar(255) NOT NULL,
  `option2` varchar(255) NOT NULL,
  `option3` varchar(255) NOT NULL,
  `option4` varchar(255) NOT NULL,
  `correct_answer` varchar(255) NOT NULL,
  `explanation` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `topic` varchar(255) NOT NULL,
  `is_active` varchar(255) NOT NULL,
  `question_image` varchar(225) DEFAULT NULL,
  `option1_image` varchar(225) DEFAULT NULL,
  `option2_image` varchar(225) DEFAULT NULL,
  `option3_image` varchar(225) DEFAULT NULL,
  `option4_image` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`id`, `question_set_id`, `question`, `option1`, `option2`, `option3`, `option4`, `correct_answer`, `explanation`, `subject`, `topic`, `is_active`, `question_image`, `option1_image`, `option2_image`, `option3_image`, `option4_image`) VALUES
(1, 1, 'g', 'g', 'fg', 'rr', 'f', 'fg', 'f', 'r', 'rr', 'rr', NULL, NULL, NULL, NULL, NULL),
(2, 1, 'das', 'sda', 'sad', 'dsa', 'dsa', 'sad', 'sad', 'dsa', 'dsa', 'das', NULL, NULL, NULL, NULL, NULL),
(3, 1, 'dsfdsfsadsad', 'fsdfsd', 'dsf', 'fsdf', 'dsf', 'fsdf', 'dfsdf', 'sdfsd', 'fsdf', 'fds', NULL, NULL, NULL, NULL, NULL),
(4, 1, 'sdsa', 'sada', 'sdsad', 'sadsa', 'dsad', 'dssad', 'sda', 'dsa', 'dsad', 'sdaasd', NULL, NULL, 'img/4/2.png', NULL, NULL),
(7, 1, 'Test question1', 'op1', '', '', '', 'op2', 'test question explanation', 'Physics', 'electronic', 'Y', 'img/7/0.png', '/home/d3sj0n2hi5oc/public_html/neetexam_image/7/1.jpeg', '/home/d3sj0n2hi5oc/public_html/neetexam_image/7/2.jpeg', '/home/d3sj0n2hi5oc/public_html/neetexam_image/7/3.jpeg', '/home/d3sj0n2hi5oc/public_html/neetexam_image/7/4.jpeg'),
(8, 1, 'test123', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'img/8/0.jpeg', 'img/8/1.jpeg', '/home/d3sj0n2hi5oc/public_html/neetexam_image/8/2.jpeg', '/home/d3sj0n2hi5oc/public_html/neetexam_image/8/3.png', '/home/d3sj0n2hi5oc/public_html/neetexam_image/8/4.jpeg'),
(9, 1, 'what is your name?', 'a', 'b', 'c', 'd', 'a', 'hey you are?', '', 'normal', '', 'img/9/0.jpeg', NULL, NULL, NULL, NULL),
(10, 1, '', '', '', '', '', '', 'img/10/5.jpeg', '', '', 'true', 'img/10/0.png', 'img/10/1.png', NULL, 'img/10/3.jpeg', 'img/10/4.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `question_set`
--

CREATE TABLE `question_set` (
  `id` int(10) NOT NULL,
  `set_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question_set`
--

INSERT INTO `question_set` (`id`, `set_name`) VALUES
(1, 'NeetExam 1'),
(6, 'NeetExam 3'),
(7, 'NeetExam4');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(10) NOT NULL,
  `contact_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `profile_url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `contact_name`, `email`, `mobile`, `password`, `city`, `profile_url`) VALUES
(1, 'sdsad', 'sad@fesdf.sd', '4324324324', '3', '', ''),
(2, 'sdsa', 'fda@fsdf.dfd', '3423432432', '3', '', ''),
(3, 'test', 'test@test.test', '1111111111', 'test', '', ''),
(4, 'sdd', 'sadas@df.ee', '4532532432', '1', '', ''),
(5, 'sadsa', 'sdsa@fgf.fgfd', '4354435435', '111', '', ''),
(6, 'Mahalingam', 'mahalingam.b@gmail.com', '8939727506', 'Mahanet78', '', ''),
(7, 'Ajay', 'ajaymandrawal17@gmail.com', '8393825792', '12345', 'dsad', ''),
(8, 'Avi Batra', 'avibatra187@gmail.com', '9997011740', 'password', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `comment` text NOT NULL,
  `rating` int(2) NOT NULL DEFAULT '0',
  `user_type` int(2) NOT NULL,
  `status` int(2) NOT NULL DEFAULT '1',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `name`, `email`, `user_id`, `comment`, `rating`, `user_type`, `status`, `created_date`) VALUES
(5, 'Mahalingam', 'mahalingam.b@gmail.com', 6, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam aliqua. Ut enim ad minim dolore magna aliqua. Ut enim ad minim veniam aliqua. Ut enim ad minim.', 4, 1, 2, '2018-09-25 08:28:22'),
(6, 'Pankaj Negi', 'mahalingam.b@gmail.com', 6, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam aliqua.', 5, 2, 2, '2018-09-25 08:32:04'),
(14, 'Shubham Uniyal', 'mahalingam.b@gmail.com', 6, 'Very Best Tutorials', 5, 2, 1, '2018-09-27 22:56:28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_time`
--
ALTER TABLE `exam_time`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `institute`
--
ALTER TABLE `institute`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_price`
--
ALTER TABLE `page_price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question_set`
--
ALTER TABLE `question_set`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `exam_time`
--
ALTER TABLE `exam_time`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `institute`
--
ALTER TABLE `institute`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `page_price`
--
ALTER TABLE `page_price`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `question`
--
ALTER TABLE `question`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `question_set`
--
ALTER TABLE `question_set`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

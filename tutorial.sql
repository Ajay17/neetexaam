-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 25, 2019 at 12:12 PM
-- Server version: 5.7.26-0ubuntu0.16.04.1
-- PHP Version: 7.0.33-5+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tutorial`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(10) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(1, 'admin', '$2a$10$xaeo5XnOGr/uh9vSOXv/..y0pFje/i3EoHeDKnjrDhy42rY.KTLVu');

-- --------------------------------------------------------

--
-- Table structure for table `exam_time`
--

CREATE TABLE `exam_time` (
  `id` int(10) NOT NULL,
  `time` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exam_time`
--

INSERT INTO `exam_time` (`id`, `time`) VALUES
(1, '2018-08-14 03:55');

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `id` int(10) NOT NULL,
  `faq_ques` varchar(255) NOT NULL,
  `faq_ans` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `faq`
--

INSERT INTO `faq` (`id`, `faq_ques`, `faq_ans`) VALUES
(1, 'hi how are you?', 'hanji'),
(8, 'hlo sir ji', 'how are you ? ok');

-- --------------------------------------------------------

--
-- Table structure for table `institute`
--

CREATE TABLE `institute` (
  `id` int(10) NOT NULL,
  `contact_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `institute_name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `institute`
--

INSERT INTO `institute` (`id`, `contact_name`, `email`, `mobile`, `password`, `address`, `city`, `website`, `institute_name`, `phone`) VALUES
(37, 'dsdsa', 'sad@fgfs.gd', '4444444444', '4', '4', '4', '4', '4', '4'),
(38, 'sda', 'dasd@fsdgf.dfgd', '2134141242', '2', '2', '2', '2', '2', '22'),
(39, 'xzcxzc', 'cxzc@fsdf.df', '3343432423', '3', '3', '3', '3', '3', '434'),
(40, 'dsadsa', 'dsf@fsd.fd', '2353532532', '3', '3', '3', '3', '3', '3'),
(41, 'test', 'test@test.test', '1111111111', 'test', 'test', 'test', 'test', 'test', '111111111');

-- --------------------------------------------------------

--
-- Table structure for table `page_price`
--

CREATE TABLE `page_price` (
  `id` int(10) NOT NULL,
  `exam_name` varchar(255) NOT NULL,
  `exam_type` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `validity` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

CREATE TABLE `question` (
  `id` int(10) NOT NULL,
  `question_set_id` int(10) NOT NULL,
  `question` varchar(255) NOT NULL,
  `option1` varchar(255) NOT NULL,
  `option2` varchar(255) NOT NULL,
  `option3` varchar(255) NOT NULL,
  `option4` varchar(255) NOT NULL,
  `correct_answer` varchar(255) NOT NULL,
  `explanation` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `topic` varchar(255) NOT NULL,
  `is_active` varchar(255) NOT NULL,
  `question_image` varchar(225) DEFAULT NULL,
  `option1_image` varchar(225) DEFAULT NULL,
  `option2_image` varchar(225) DEFAULT NULL,
  `option3_image` varchar(225) DEFAULT NULL,
  `option4_image` varchar(225) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`id`, `question_set_id`, `question`, `option1`, `option2`, `option3`, `option4`, `correct_answer`, `explanation`, `subject`, `topic`, `is_active`, `question_image`, `option1_image`, `option2_image`, `option3_image`, `option4_image`) VALUES
(1, 1, 'g', 'g', 'fg', 'rr', 'f', 'fg', 'f', 'r', 'rr', 'rr', NULL, NULL, NULL, NULL, NULL),
(2, 1, 'das', 'sda', 'sad', 'dsa', 'dsa', 'sad', 'sad', 'dsa', 'dsa', 'das', NULL, NULL, NULL, NULL, NULL),
(3, 1, 'dsfdsfsadsad', 'fsdfsd', 'dsf', 'fsdf', 'dsf', 'fsdf', 'dfsdf', 'sdfsd', 'fsdf', 'fds', NULL, NULL, NULL, NULL, NULL),
(4, 1, 'sdsa', 'sada', 'sdsad', 'sadsa', 'dsad', 'dssad', 'sda', 'dsa', 'dsad', 'sdaasd', NULL, NULL, 'img/4/2.png', NULL, NULL),
(7, 1, 'Test question1', 'op1', '', '', '', 'op2', 'test question explanation', 'Physics', 'electronic', 'Y', 'img/7/0.png', '/home/d3sj0n2hi5oc/public_html/neetexam_image/7/1.jpeg', '/home/d3sj0n2hi5oc/public_html/neetexam_image/7/2.jpeg', '/home/d3sj0n2hi5oc/public_html/neetexam_image/7/3.jpeg', '/home/d3sj0n2hi5oc/public_html/neetexam_image/7/4.jpeg'),
(8, 1, 'test123', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'test', 'img/8/0.jpeg', 'img/8/1.jpeg', '/home/d3sj0n2hi5oc/public_html/neetexam_image/8/2.jpeg', '/home/d3sj0n2hi5oc/public_html/neetexam_image/8/3.png', '/home/d3sj0n2hi5oc/public_html/neetexam_image/8/4.jpeg'),
(9, 1, 'what is your name?', 'a', 'b', 'c', 'd', 'a', 'hey you are?', '', 'normal', '', 'img/9/0.jpeg', NULL, NULL, NULL, NULL),
(10, 1, '', '', '', '', '', '', 'img/10/5.jpeg', '', '', 'true', 'img/10/0.png', 'img/10/1.png', NULL, 'img/10/3.jpeg', 'img/10/4.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `question_set`
--

CREATE TABLE `question_set` (
  `id` int(10) NOT NULL,
  `set_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `question_set`
--

INSERT INTO `question_set` (`id`, `set_name`) VALUES
(1, 'NeetExam 1'),
(6, 'NeetExam 3'),
(7, 'NeetExam4');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(10) NOT NULL,
  `contact_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `profile_url` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `contact_name`, `email`, `mobile`, `password`, `city`, `profile_url`) VALUES
(1, 'sdsad', 'sad@fesdf.sd', '4324324324', '3', '', ''),
(2, 'sdsa', 'fda@fsdf.dfd', '3423432432', '3', '', ''),
(3, 'test', 'test@test.test', '1111111111', 'test', '', ''),
(4, 'sdd', 'sadas@df.ee', '4532532432', '1', '', ''),
(5, 'sadsa', 'sdsa@fgf.fgfd', '4354435435', '111', '', ''),
(6, 'Mahalingam', 'mahalingam.b@gmail.com', '8939727506', 'Mahanet78', '', ''),
(7, 'Ajay', 'ajaymandrawal17@gmail.com', '8393825792', '12345', 'dsad', ''),
(8, 'Avi Batra', 'avibatra187@gmail.com', '9997011740', 'password', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `sub_category`
--

CREATE TABLE `sub_category` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `subtitle` mediumtext NOT NULL,
  `description` mediumtext NOT NULL,
  `youtube_link` varchar(300) NOT NULL,
  `content` longtext NOT NULL,
  `admin_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sub_category`
--

INSERT INTO `sub_category` (`id`, `category_id`, `title`, `subtitle`, `description`, `youtube_link`, `content`, `admin_id`, `created_at`, `modified_at`) VALUES
(1, 1, 'Android Custom Dialog Example â€“ Making Custom AlertDialog', 'android', '<h2>Android Custom Dialog Example</h2><ul>	<li>Again the first thing is creating a new Android Studio project. You can implement the same in your existing project too.</li>	<li>I have created a project named&nbsp;<strong>Android Custom Dialog</strong>.</li>	<li>First come inside&nbsp;<strong>colors.xml</strong>&nbsp;and change the default colors to the following.</li></ul><p>&nbsp;</p><p>colors.xml</p><p><textarea></textarea></p><table>	<tbody>		<tr>			<td>			<p>1</p>			<p>2</p>			<p>3</p>			<p>4</p>			<p>5</p>			<p>6</p>			<p>7</p>			<p>8</p>			<p>9</p>			<p>10</p>			<p>11</p>			</td>			<td>			<p>&lt;resources&gt;</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;!-- Base application theme. --&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;style name=&quot;AppTheme&quot; parent=&quot;Theme.AppCompat.Light.DarkActionBar&quot;&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;!-- Customize your theme here. --&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;item name=&quot;colorPrimary&quot;&gt;@color/colorPrimary&lt;/item&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;item name=&quot;colorPrimaryDark&quot;&gt;@color/colorPrimaryDark&lt;/item&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;item name=&quot;colorAccent&quot;&gt;@color/colorAccent&lt;/item&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;/style&gt;</p>			<p>&nbsp;</p>			<p>&lt;/resources&gt;</p>			</td>		</tr>	</tbody></table><p>&nbsp;</p><ul>	<li>Now below you can see the design of the Custom Dialog that I am going to create.</li></ul><p><img alt="Android Custom Dialog Example" src="https://i1.wp.com/www.simplifiedcoding.net/wp-content/uploads/2018/09/Android-Custom-Dialog-Example.png?resize=296%2C497&amp;ssl=1" style="height:497px; width:296px" /></p><p>Android Custom Dialog Example</p><ul>	<li>For the above design we have defined the colors, now we need the background of the OK button and the icon for the header.</li>	<li>For the button background just create a new drawable resource file named&nbsp;<strong>button_background.xml</strong>and put the following code.</li></ul><p>&nbsp;</p><p>button_background.xml</p><p><textarea></textarea></p><table>	<tbody>		<tr>			<td>			<p>1</p>			<p>2</p>			<p>3</p>			<p>4</p>			<p>5</p>			<p>6</p>			<p>7</p>			<p>8</p>			<p>9</p>			<p>10</p>			<p>11</p>			<p>12</p>			<p>13</p>			<p>14</p>			</td>			<td>			<p>&lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;</p>			<p>&lt;selector xmlns:android=&quot;http://schemas.android.com/apk/res/android&quot;&gt;</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;item&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;shape&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;stroke android:width=&quot;2dp&quot; android:color=&quot;@color/colorPrimary&quot; /&gt;</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;solid android:color=&quot;@android:color/transparent&quot; /&gt;</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;corners android:bottomLeftRadius=&quot;6dp&quot; android:bottomRightRadius=&quot;6dp&quot; android:topLeftRadius=&quot;6dp&quot; android:topRightRadius=&quot;6dp&quot; /&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/shape&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;/item&gt;</p>			<p>&nbsp;</p>			<p>&lt;/selector&gt;</p>			</td>		</tr>	</tbody></table><p>&nbsp;</p><ul>	<li>For the icon that is displaying in the header, create one more drawable file. I created&nbsp;<strong>ic_success.xml&nbsp;</strong>and put the following code inside.</li></ul><p>&nbsp;</p><p>ic_success.xml</p><p><textarea></textarea></p><table>	<tbody>		<tr>			<td>			<p>1</p>			<p>2</p>			<p>3</p>			<p>4</p>			<p>5</p>			</td>			<td>			<p>&lt;vector android:height=&quot;24dp&quot; android:viewportHeight=&quot;512&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;android:viewportWidth=&quot;512&quot; android:width=&quot;24dp&quot; xmlns:android=&quot;http://schemas.android.com/apk/res/android&quot;&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;path android:fillColor=&quot;#FFFFFF&quot; android:pathData=&quot;M468.907,214.604c-11.423,0 -20.682,9.26 -20.682,20.682v20.831c-0.031,54.338 -21.221,105.412 -59.666,143.812c-38.417,38.372 -89.467,59.5 -143.761,59.5c-0.04,0 -0.08,0 -0.12,0C132.506,459.365 41.3,368.056 41.364,255.883c0.031,-54.337 21.221,-105.411 59.667,-143.813c38.417,-38.372 89.468,-59.5 143.761,-59.5c0.04,0 0.08,0 0.12,0c28.672,0.016 56.49,5.942 82.68,17.611c10.436,4.65 22.659,-0.041 27.309,-10.474c4.648,-10.433 -0.04,-22.659 -10.474,-27.309c-31.516,-14.043 -64.989,-21.173 -99.492,-21.192c-0.052,0 -0.092,0 -0.144,0c-65.329,0 -126.767,25.428 -172.993,71.6C25.536,129.014 0.038,190.473 0,255.861c-0.037,65.386 25.389,126.874 71.599,173.136c46.21,46.262 107.668,71.76 173.055,71.798c0.051,0 0.092,0 0.144,0c65.329,0 126.767,-25.427 172.993,-71.6c46.262,-46.209 71.76,-107.668 71.798,-173.066v-20.842C489.589,223.864 480.33,214.604 468.907,214.604z&quot;/&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;path android:fillColor=&quot;#FFFFFF&quot; android:pathData=&quot;M505.942,39.803c-8.077,-8.076 -21.172,-8.076 -29.249,0L244.794,271.701l-52.609,-52.609c-8.076,-8.077 -21.172,-8.077 -29.248,0c-8.077,8.077 -8.077,21.172 0,29.249l67.234,67.234c4.038,4.039 9.332,6.058 14.625,6.058c5.293,0 10.586,-2.019 14.625,-6.058L505.942,69.052C514.019,60.975 514.019,47.88 505.942,39.803z&quot;/&gt;</p>			<p>&lt;/vector&gt;</p>			</td>		</tr>	</tbody></table><p>&nbsp;</p><ul>	<li>You can also use any other icon if you want.</li>	<li>Now for our Custom Alert Dialog create a layout. I have created&nbsp;<strong>my_dialog.xml</strong>. Inside this file we will design our&nbsp;<strong>Android Custom Dialog</strong>.</li></ul><p>&nbsp;</p><p>my_dialog.xml</p><p><textarea></textarea></p><table>	<tbody>		<tr>			<td>			<p>1</p>			<p>2</p>			<p>3</p>			<p>4</p>			<p>5</p>			<p>6</p>			<p>7</p>			<p>8</p>			<p>9</p>			<p>10</p>			<p>11</p>			<p>12</p>			<p>13</p>			<p>14</p>			<p>15</p>			<p>16</p>			<p>17</p>			<p>18</p>			<p>19</p>			<p>20</p>			<p>21</p>			<p>22</p>			<p>23</p>			<p>24</p>			<p>25</p>			<p>26</p>			<p>27</p>			<p>28</p>			<p>29</p>			<p>30</p>			<p>31</p>			<p>32</p>			<p>33</p>			<p>34</p>			<p>35</p>			<p>36</p>			<p>37</p>			<p>38</p>			<p>39</p>			<p>40</p>			<p>41</p>			<p>42</p>			<p>43</p>			<p>44</p>			<p>45</p>			<p>46</p>			<p>47</p>			<p>48</p>			<p>49</p>			<p>50</p>			<p>51</p>			<p>52</p>			<p>53</p>			</td>			<td>			<p>&lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;</p>			<p>&lt;LinearLayout xmlns:android=&quot;http://schemas.android.com/apk/res/android&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;android:layout_width=&quot;match_parent&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;android:layout_height=&quot;match_parent&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;android:orientation=&quot;vertical&quot;&gt;</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;RelativeLayout</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:layout_width=&quot;match_parent&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:layout_height=&quot;80dp&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:background=&quot;@color/colorPrimary&quot;&gt;</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;ImageView</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:layout_width=&quot;50dp&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:layout_height=&quot;50dp&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:layout_centerInParent=&quot;true&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:background=&quot;@drawable/ic_success&quot; /&gt;</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;/RelativeLayout&gt;</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;LinearLayout</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:layout_width=&quot;match_parent&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:layout_height=&quot;wrap_content&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:orientation=&quot;vertical&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:padding=&quot;16dp&quot;&gt;</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;TextView</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:layout_width=&quot;match_parent&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:layout_height=&quot;wrap_content&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:text=&quot;Success&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:textAlignment=&quot;center&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:textAppearance=&quot;@style/TextAppearance.AppCompat.Headline&quot; /&gt;</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;TextView</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:layout_width=&quot;match_parent&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:layout_height=&quot;wrap_content&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:layout_marginTop=&quot;10dp&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:text=&quot;Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla eu erat tincidunt lacus fermentum rutrum.&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:textAlignment=&quot;center&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:textAppearance=&quot;@style/TextAppearance.AppCompat.Medium&quot; /&gt;</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;Button</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:id=&quot;@+id/buttonOk&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:layout_width=&quot;200dp&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:layout_height=&quot;wrap_content&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:layout_gravity=&quot;center&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:layout_marginTop=&quot;15dp&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:background=&quot;@drawable/button_background&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:text=&quot;Ok&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:textColor=&quot;@color/colorPrimary&quot; /&gt;</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;/LinearLayout&gt;</p>			<p>&nbsp;</p>			<p>&lt;/LinearLayout&gt;</p>			</td>		</tr>	</tbody></table><p>&nbsp;</p><p><ins><ins><ins><iframe frameborder="0" height="90" id="aswift_2" name="aswift_2" scrolling="no" width="809"></iframe></ins></ins></ins></p><ul>	<li>The above layout code will generate the following design.</li></ul><p><img alt="Android Custom Dialog Example" src="https://i1.wp.com/www.simplifiedcoding.net/wp-content/uploads/2018/09/Android-Custom-Dialog-Example.png?resize=296%2C497&amp;ssl=1" style="height:497px; width:296px" /></p><p>Android Custom Dialog Example</p><ul>	<li>Now we need to display the above Custom Dialog Design as an Alert Dialog when a button is clicked.</li>	<li>For this come inside&nbsp;<strong>activity_main.xml</strong>&nbsp;and create a button inside the activity.</li></ul><p>&nbsp;</p><p>activity_main.xml</p><p><textarea></textarea></p><table>	<tbody>		<tr>			<td>			<p>1</p>			<p>2</p>			<p>3</p>			<p>4</p>			<p>5</p>			<p>6</p>			<p>7</p>			<p>8</p>			<p>9</p>			<p>10</p>			<p>11</p>			<p>12</p>			<p>13</p>			<p>14</p>			<p>15</p>			<p>16</p>			<p>17</p>			<p>18</p>			<p>19</p>			<p>20</p>			<p>21</p>			</td>			<td>			<p>&lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;</p>			<p>&lt;RelativeLayout xmlns:android=&quot;http://schemas.android.com/apk/res/android&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;xmlns:app=&quot;http://schemas.android.com/apk/res-auto&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;xmlns:tools=&quot;http://schemas.android.com/tools&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;android:layout_width=&quot;match_parent&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;android:layout_height=&quot;match_parent&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;tools:context=&quot;.MainActivity&quot;&gt;</p>			<p>&nbsp;</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;Button</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:id=&quot;@+id/buttonShowDialog&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:layout_width=&quot;wrap_content&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:layout_height=&quot;wrap_content&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:layout_centerInParent=&quot;true&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:background=&quot;@color/colorPrimary&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:padding=&quot;15dp&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:text=&quot;Android Custom Dialog Example&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:textAllCaps=&quot;false&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:textColor=&quot;#ffffff&quot; /&gt;</p>			<p>&nbsp;</p>			<p>&lt;/RelativeLayout&gt;</p>			</td>		</tr>	</tbody></table><p>&nbsp;</p><ul>	<li>So your&nbsp;<strong>activity_main.xml</strong>&nbsp;will look as below.</li></ul><p><img alt="Android Custom Dialog" src="https://i1.wp.com/www.simplifiedcoding.net/wp-content/uploads/2018/09/Android-Custom-Dialog.png?resize=304%2C499&amp;ssl=1" style="height:499px; width:304px" /></p><p>Android Custom Dialog</p><ul>	<li>Now the last task is to display our Custom Dialog when the button is clicked.</li>	<li>For displaying the dialog come inside&nbsp;<strong>MainActivity.java</strong>&nbsp;and write the following code.</li></ul><p>&nbsp;</p><p>MainActivity.java</p><p>Java</p><p><textarea></textarea></p><table>	<tbody>		<tr>			<td>			<p>1</p>			<p>2</p>			<p>3</p>			<p>4</p>			<p>5</p>			<p>6</p>			<p>7</p>			<p>8</p>			<p>9</p>			<p>10</p>			<p>11</p>			<p>12</p>			<p>13</p>			<p>14</p>			<p>15</p>			<p>16</p>			<p>17</p>			<p>18</p>			<p>19</p>			<p>20</p>			<p>21</p>			<p>22</p>			<p>23</p>			<p>24</p>			<p>25</p>			<p>26</p>			<p>27</p>			<p>28</p>			<p>29</p>			<p>30</p>			<p>31</p>			<p>32</p>			<p>33</p>			<p>34</p>			<p>35</p>			<p>36</p>			<p>37</p>			<p>38</p>			<p>39</p>			<p>40</p>			<p>41</p>			<p>42</p>			<p>43</p>			<p>44</p>			<p>45</p>			<p>46</p>			</td>			<td>			<p>package net.simplifiedcoding.androidcustomdialog;</p>			<p>&nbsp;</p>			<p>import android.support.v7.app.AlertDialog;</p>			<p>import android.support.v7.app.AppCompatActivity;</p>			<p>import android.os.Bundle;</p>			<p>import android.view.LayoutInflater;</p>			<p>import android.view.View;</p>			<p>import android.view.ViewGroup;</p>			<p>&nbsp;</p>			<p>public class MainActivity extends AppCompatActivity {</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;@Override</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;protected void onCreate(Bundle savedInstanceState) {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;super.onCreate(savedInstanceState);</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;setContentView(R.layout.activity_main);</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;//attaching click listener</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;findViewById(R.id.buttonShowDialog).setOnClickListener(new View.OnClickListener() {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@Override</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;public void onClick(View v) {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;//calling this method to show our android custom alert dialog</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;showCustomDialog();</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;});</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;}</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;private void showCustomDialog() {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;//before inflating the custom alert dialog layout, we will get the current activity viewgroup</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ViewGroup viewGroup = findViewById(android.R.id.content);</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;//then we will inflate the custom alert dialog xml that we created</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;View dialogView = LayoutInflater.from(this).inflate(R.layout.my_dialog, viewGroup, false);</p>			<p>&nbsp;</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;//Now we need an AlertDialog.Builder object</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AlertDialog.Builder builder = new AlertDialog.Builder(this);</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;//setting the view of the builder to our custom view that we already inflated</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;builder.setView(dialogView);</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;//finally creating the alert dialog and displaying it</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AlertDialog alertDialog = builder.create();</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alertDialog.show();</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;}</p>			<p>}</p>			</td>		</tr>	</tbody></table>', '', '<p>In this post we will see an&nbsp;<strong>Android Custom Dialog Example</strong>. I hope you know about creating Dialogs in Android, and then you might be thinking that what if I can customized the dialog completely as per my need. Then the answer is &ldquo;YES&rdquo; you can customized it as you want, you can put whatever design or component you need in your Alert Dialog.</p>', 1, '2019-05-24 15:43:45', '2019-05-24 15:43:45'),
(2, 1, 'Android Splash Screen Example â€“ Implementing Splash Screen', 'Android', '<h2>Android Splash Screen Example</h2><p>For building the Splash Screen the first thing needed is the design of the screen. But as this post is only an example we will not dig much into the designing part. Here I am going to use only a logo that I will display in the center of the screen.</p><p>Now let&rsquo;s just create our android project.</p><ul>	<li>Here I have created a new project using an&nbsp;<strong>EmptyActivity</strong>.</li>	<li>Once your project is loaded, first we will design our Splash Screen.</li></ul><h3>Designing Splash Screen</h3><p>You can play with the design as much as you want. Here I am just using logo of StackOverflow to create a very quick design. It is having nothing just the logo image on the center.</p><p>For designing the Splash Screen we will create a drawable resource file.</p><ul>	<li>Right click on the drawable folder and create a new drawable resource file. Here I have created&nbsp;<strong>splash_screen.xml</strong>.</li></ul><p><img alt="android splash screen example" src="https://i0.wp.com/www.simplifiedcoding.net/wp-content/uploads/2018/09/android-splash-screen-example.png?resize=792%2C804&amp;ssl=1" style="height:804px; width:792px" /></p><p>Creating Drawable Resource File</p><ul>	<li>Here we are going to design our Splash Screen. So inside the file put the following code.</li></ul><p>&nbsp;</p><p>splash_screen.xml</p><p><textarea></textarea></p><table>	<tbody>		<tr>			<td>			<p>1</p>			<p>2</p>			<p>3</p>			<p>4</p>			<p>5</p>			<p>6</p>			<p>7</p>			<p>8</p>			<p>9</p>			<p>10</p>			<p>11</p>			<p>12</p>			<p>13</p>			<p>14</p>			<p>15</p>			</td>			<td>			<p>&lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;</p>			<p>&lt;layer-list xmlns:android=&quot;http://schemas.android.com/apk/res/android&quot;&gt;</p>			<p>&nbsp;</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;item&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;color android:color=&quot;#ffffff&quot;&gt;&lt;/color&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;/item&gt;</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;item</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:width=&quot;170dp&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:height=&quot;180dp&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:drawable=&quot;@drawable/app_logo&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:gravity=&quot;center_horizontal|center_vertical&quot; /&gt;</p>			<p>&nbsp;</p>			<p>&lt;/layer-list&gt;</p>			</td>		</tr>	</tbody></table><p>&nbsp;</p><p><ins><ins><ins><iframe frameborder="0" height="90" id="aswift_2" name="aswift_2" scrolling="no" width="809"></iframe></ins></ins></ins></p><ul>	<li>In the above code&nbsp;<strong>app_logo</strong>&nbsp;is the image file that I have already pasted inside my drawable folder, you need to put your logo that you want in the splash screen.</li>	<li>Now we will create a theme that will show this designed screen as the background of our activity.</li></ul><h3>Creating Splash Theme</h3><ul>	<li>Open the file<strong>&nbsp;res-&gt;values-&gt;styles.xml</strong>&nbsp;and modify it as below.</li></ul><p>&nbsp;</p><p>styles.xml</p><p><textarea></textarea></p><table>	<tbody>		<tr>			<td>			<p>1</p>			<p>2</p>			<p>3</p>			<p>4</p>			<p>5</p>			<p>6</p>			<p>7</p>			<p>8</p>			<p>9</p>			<p>10</p>			<p>11</p>			<p>12</p>			<p>13</p>			<p>14</p>			<p>15</p>			<p>16</p>			<p>17</p>			<p>18</p>			</td>			<td>			<p>&lt;resources&gt;</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;!-- Base application theme. --&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;style name=&quot;AppTheme&quot; parent=&quot;Theme.AppCompat.Light.DarkActionBar&quot;&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;!-- Customize your theme here. --&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;item name=&quot;colorPrimary&quot;&gt;@color/colorPrimary&lt;/item&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;item name=&quot;colorPrimaryDark&quot;&gt;@color/colorPrimaryDark&lt;/item&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;item name=&quot;colorAccent&quot;&gt;@color/colorAccent&lt;/item&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;/style&gt;</p>			<p>&nbsp;</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;!-- Android Splash Screen Example --&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;style name=&quot;AppTheme.Launcher&quot; parent=&quot;Theme.AppCompat.Light.NoActionBar&quot;&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;item name=&quot;android:windowBackground&quot;&gt;@drawable/splash_screen&lt;/item&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;/style&gt;</p>			<p>&nbsp;</p>			<p>&lt;/resources&gt;</p>			</td>		</tr>	</tbody></table><p>&nbsp;</p><ul>	<li>So we have a theme that will show up our Splash Screen. As you can see for the&nbsp;<strong>windowBackground</strong>&nbsp;of the theme we have set the&nbsp;<strong>splash_screen.xml</strong>.</li></ul><h3>Displaying Splash Screen</h3><p>Let me first tell you the idea. The splash screen will show up only when the activity is not loaded, once the activity is loaded the splash screen will disappear. That is why we have set the splash screen as the windowBackground, so it will so the splash screen only when the activity is not loaded.</p><ul>	<li>To achieve this objective we will change our MainActivity theme with the theme we created. So come inside AndroidManifest.xml and change the theme to<strong>&nbsp;App.Launcher</strong>, as shown in the below code.</li></ul><p>&nbsp;</p><p>AndroidManifest.xml</p><p><textarea></textarea></p><table>	<tbody>		<tr>			<td>			<p>1</p>			<p>2</p>			<p>3</p>			<p>4</p>			<p>5</p>			<p>6</p>			<p>7</p>			<p>8</p>			<p>9</p>			<p>10</p>			<p>11</p>			<p>12</p>			<p>13</p>			<p>14</p>			<p>15</p>			<p>16</p>			<p>17</p>			<p>18</p>			<p>19</p>			<p>20</p>			<p>21</p>			</td>			<td>			<p>&lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;</p>			<p>&lt;manifest xmlns:android=&quot;http://schemas.android.com/apk/res/android&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;package=&quot;net.simplifiedcoding.androidsplashscreen&quot;&gt;</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;application</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:allowBackup=&quot;true&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:icon=&quot;@mipmap/ic_launcher&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:label=&quot;@string/app_name&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:roundIcon=&quot;@mipmap/ic_launcher_round&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:supportsRtl=&quot;true&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:theme=&quot;@style/AppTheme.Launcher&quot;&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;activity android:name=&quot;.MainActivity&quot;&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;intent-filter&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;action android:name=&quot;android.intent.action.MAIN&quot; /&gt;</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;category android:name=&quot;android.intent.category.LAUNCHER&quot; /&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/intent-filter&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/activity&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;/application&gt;</p>			<p>&nbsp;</p>			<p>&lt;/manifest&gt;</p>			</td>		</tr>	</tbody></table><p>&nbsp;</p><ul>	<li>Now when we launch the activity it will show the Splash Screen, and when the activity is loaded we will change the theme and then it will display our Activity.</li>	<li>So come inside&nbsp;<strong>MainActivity.java</strong>&nbsp;and change the theme inside the method&nbsp;<strong>onCreate()</strong>.</li></ul><p>&nbsp;</p><p>MainActivity.java</p><p>Java</p><p><textarea></textarea></p><table>	<tbody>		<tr>			<td>			<p>1</p>			<p>2</p>			<p>3</p>			<p>4</p>			<p>5</p>			<p>6</p>			<p>7</p>			<p>8</p>			<p>9</p>			<p>10</p>			<p>11</p>			<p>12</p>			<p>13</p>			<p>14</p>			<p>15</p>			<p>16</p>			<p>17</p>			<p>18</p>			<p>19</p>			</td>			<td>			<p>package net.simplifiedcoding.androidsplashscreen;</p>			<p>&nbsp;</p>			<p>import android.support.v7.app.AppCompatActivity;</p>			<p>import android.os.Bundle;</p>			<p>&nbsp;</p>			<p>public class MainActivity extends AppCompatActivity {</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;@Override</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;protected void onCreate(Bundle savedInstanceState) {</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;//Setting the AppTheme to display the activity</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;setTheme(R.style.AppTheme);</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;super.onCreate(savedInstanceState);</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;setContentView(R.layout.activity_main);</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;//then you can do the rest of the activity thing</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;}</p>			<p>}</p>			</td>		</tr>	</tbody></table><p>&nbsp;</p><ul>	<li>Now run the application and you will see your designed splash screen before loading the activity. It may be too quick if your device is very fast.</li></ul><p>So that is all for this Android Splash Screen Example friends. I hope you found it helpful. For any kind of question you can leave your comments.</p><p>And if you think the post is helpful please share this post with your friends. Thank You ðŸ™‚</p><p>&nbsp;</p>', '', '<p>Hi everyone, welcome to another useful tutorial for beginners. So this post will show you an&nbsp;<strong>Android Splash Screen Example</strong>. An splash screen is a screen that shows up when we launch the app. Splash Screen is very common term and it is required for all the application. The idea behind this is, to show user a welcome screen while your application is loading.</p><p>In this android splash screen example we are going to learn how do we implement a splash screen in our application. So let&rsquo;s begin.</p>', 1, '2019-05-24 15:50:05', '2019-05-24 15:50:05');
INSERT INTO `sub_category` (`id`, `category_id`, `title`, `subtitle`, `description`, `youtube_link`, `content`, `admin_id`, `created_at`, `modified_at`) VALUES
(3, 1, 'Turn on Flashlight Android â€“ Building a Torch App', 'Android', '<h2>Torch Application Android Project</h2><ul>	<li>The first thing, as always we need our new project. So open Android Studio and create a new project with an Empty Activity.</li></ul><p><img alt="turn on flashlight android" src="https://i2.wp.com/www.simplifiedcoding.net/wp-content/uploads/2018/07/turn-on-flashlight-android.png?resize=900%2C280&amp;ssl=1" style="height:252px; width:809px" /></p><p>Turn On Flashlight Android</p><ul>	<li>Make sure you select API 23, as the minimum API. This is because the API that we are going to use is available on API 23 and later only, and the older one is deprecated. So to make things clean and easy to understand we are targeting only API 23 and later.</li></ul><h2>Creating Interface</h2><ul>	<li>Here you can play with the design, you can put nice images and you can make your screen look awesome. But the fun part is I am not going to bother about design, and yes I will create probably the worse UI. haha .</li>	<li>So come inside&nbsp;<strong>activity_main.xml</strong>&nbsp;and write the following code.</li></ul><p>&nbsp;</p><p>activity_main.xml</p><p><textarea></textarea></p><table>	<tbody>		<tr>			<td>			<p>1</p>			<p>2</p>			<p>3</p>			<p>4</p>			<p>5</p>			<p>6</p>			<p>7</p>			<p>8</p>			<p>9</p>			<p>10</p>			<p>11</p>			<p>12</p>			<p>13</p>			<p>14</p>			<p>15</p>			<p>16</p>			<p>17</p>			<p>18</p>			<p>19</p>			</td>			<td>			<p>&lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;</p>			<p>&lt;android.support.constraint.ConstraintLayout xmlns:android=&quot;http://schemas.android.com/apk/res/android&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;xmlns:app=&quot;http://schemas.android.com/apk/res-auto&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;xmlns:tools=&quot;http://schemas.android.com/tools&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;android:layout_width=&quot;match_parent&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;android:layout_height=&quot;match_parent&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;tools:context=&quot;.MainActivity&quot;&gt;</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;ToggleButton</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:id=&quot;@+id/toggleButton&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:layout_width=&quot;wrap_content&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:layout_height=&quot;wrap_content&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:text=&quot;ToggleButton&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;app:layout_constraintBottom_toBottomOf=&quot;parent&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;app:layout_constraintEnd_toEndOf=&quot;parent&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;app:layout_constraintStart_toStartOf=&quot;parent&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;app:layout_constraintTop_toTopOf=&quot;parent&quot; /&gt;</p>			<p>&nbsp;</p>			<p>&lt;/android.support.constraint.ConstraintLayout&gt;</p>			</td>		</tr>	</tbody></table><p>&nbsp;</p><ul>	<li>The above XML will give you the following output. We only have a ToggleButton that we will use to turn on or off the android flash light.</li></ul><p><img alt="turn on flashlight android" src="https://i1.wp.com/www.simplifiedcoding.net/wp-content/uploads/2018/07/turn-on-flashlight-android-1.png?resize=311%2C512&amp;ssl=1" style="height:512px; width:311px" /></p><p>Turn on Flashlight Android</p><ul>	<li>So here we have one of the worse UI design of all time. ðŸ˜‰ LOL. Design if you want, or lets dive into adding the functionality.</li></ul><h2>Adding Feature in Manifest</h2><ul>	<li>As we are going to build a Torch application, and basically we want to programmatically turn on the flashlight of our android device. To do this we need to access this feature of android.</li>	<li>So come inside&nbsp;<strong>AndroidManifest.xml</strong>&nbsp;and add the line shown below.</li></ul><p>&nbsp;</p><p>AndroidManifest.xml</p><p><textarea></textarea></p><table>	<tbody>		<tr>			<td>			<p>1</p>			<p>2</p>			<p>3</p>			<p>4</p>			<p>5</p>			<p>6</p>			<p>7</p>			<p>8</p>			<p>9</p>			<p>10</p>			<p>11</p>			<p>12</p>			<p>13</p>			<p>14</p>			<p>15</p>			<p>16</p>			<p>17</p>			<p>18</p>			<p>19</p>			<p>20</p>			<p>21</p>			<p>22</p>			<p>23</p>			<p>24</p>			</td>			<td>			<p>&lt;?xml version=&quot;1.0&quot; encoding=&quot;utf-8&quot;?&gt;</p>			<p>&lt;manifest xmlns:android=&quot;http://schemas.android.com/apk/res/android&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;package=&quot;net.simplifiedcoding.mytorch&quot;&gt;</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;!-- add this line that means our application will be using the device flash --&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;uses-feature android:name=&quot;android.hardware.camera.flash&quot; /&gt;</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;application</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:allowBackup=&quot;true&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:icon=&quot;@mipmap/ic_launcher&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:label=&quot;@string/app_name&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:roundIcon=&quot;@mipmap/ic_launcher_round&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:supportsRtl=&quot;true&quot;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;android:theme=&quot;@style/AppTheme&quot;&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;activity android:name=&quot;.MainActivity&quot;&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;intent-filter&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;action android:name=&quot;android.intent.action.MAIN&quot; /&gt;</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;category android:name=&quot;android.intent.category.LAUNCHER&quot; /&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/intent-filter&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lt;/activity&gt;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&lt;/application&gt;</p>			<p>&nbsp;</p>			<p>&lt;/manifest&gt;</p>			</td>		</tr>	</tbody></table><p>&nbsp;</p><p><ins><ins><ins><iframe frameborder="0" height="90" id="aswift_2" name="aswift_2" scrolling="no" width="809"></iframe></ins></ins></ins></p><h2>Building Basic Code Structure</h2><ul>	<li>We don&rsquo;t have much to do, we only have a ToggleButton where we need to attach a listener to listen for the on and off events. To do this we will write the following code.</li></ul><p>&nbsp;</p><p><textarea></textarea></p><table>	<tbody>		<tr>			<td>			<p>1</p>			<p>2</p>			<p>3</p>			<p>4</p>			<p>5</p>			<p>6</p>			<p>7</p>			<p>8</p>			<p>9</p>			<p>10</p>			<p>11</p>			<p>12</p>			<p>13</p>			<p>14</p>			<p>15</p>			<p>16</p>			<p>17</p>			<p>18</p>			<p>19</p>			<p>20</p>			</td>			<td>			<p>public class MainActivity extends AppCompatActivity {</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;private ToggleButton toggleButton;</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;@Override</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;protected void onCreate(Bundle savedInstanceState) {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;super.onCreate(savedInstanceState);</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;setContentView(R.layout.activity_main);</p>			<p>&nbsp;</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;toggleButton = findViewById(R.id.toggleButton);</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@Override</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; //called when the button status is changed&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;});</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;}</p>			</td>		</tr>	</tbody></table><p>&nbsp;</p><h2>Checking if Flash is Available</h2><ul>	<li>Now let&rsquo;s write some java code. When our application will enter the&nbsp;<strong>onCreate()</strong>&nbsp;method, first we will check if the device has a flash light or not. If the device do not have flashlight we will quit the application saying the flash is not available.</li>	<li>So our&nbsp;<strong>onCreate()</strong>&nbsp;method will be like this.</li></ul><p>&nbsp;</p><p>Java</p><p><textarea></textarea></p><table>	<tbody>		<tr>			<td>			<p>1</p>			<p>2</p>			<p>3</p>			<p>4</p>			<p>5</p>			<p>6</p>			<p>7</p>			<p>8</p>			<p>9</p>			<p>10</p>			<p>11</p>			<p>12</p>			<p>13</p>			<p>14</p>			<p>15</p>			<p>16</p>			<p>17</p>			<p>18</p>			<p>19</p>			<p>20</p>			<p>21</p>			</td>			<td>			<p>&nbsp;&nbsp;&nbsp;&nbsp;@Override</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;protected void onCreate(Bundle savedInstanceState) {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;super.onCreate(savedInstanceState);</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;setContentView(R.layout.activity_main);</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;boolean isFlashAvailable = getApplicationContext().getPackageManager()</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if (!isFlashAvailable) {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;showNoFlashError();</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;toggleButton = findViewById(R.id.toggleButton);</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@Override</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;});</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;}</p>			</td>		</tr>	</tbody></table><p>&nbsp;</p><ul>	<li>We are calling the method&nbsp;<strong>showNoFlashError()</strong>&nbsp;that you can define as below.</li></ul><p>&nbsp;</p><p>Java</p><p><textarea></textarea></p><table>	<tbody>		<tr>			<td>			<p>1</p>			<p>2</p>			<p>3</p>			<p>4</p>			<p>5</p>			<p>6</p>			<p>7</p>			<p>8</p>			<p>9</p>			<p>10</p>			<p>11</p>			<p>12</p>			</td>			<td>			<p>&nbsp;&nbsp;&nbsp;&nbsp;public void showNoFlashError() {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AlertDialog alert = new AlertDialog.Builder(this)</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.create();</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert.setTitle(&quot;Oops!&quot;);</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert.setMessage(&quot;Flash not available in this device...&quot;);</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert.setButton(DialogInterface.BUTTON_POSITIVE, &quot;OK&quot;, new DialogInterface.OnClickListener() {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;public void onClick(DialogInterface dialog, int which) {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;finish();</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;});</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert.show();</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;}</p>			</td>		</tr>	</tbody></table><p>&nbsp;</p><h2>Getting Camera Manager</h2><ul>	<li>Now we need&nbsp;<strong>CameraManager</strong>, and a&nbsp;<strong>String</strong>&nbsp;for the&nbsp;<strong>Camera Id</strong>, as most devices has multiple cameras.</li></ul><p>&nbsp;</p><p>Java</p><p><textarea></textarea></p><table>	<tbody>		<tr>			<td>			<p>1</p>			<p>2</p>			<p>3</p>			</td>			<td>			<p>public class MainActivity extends AppCompatActivity {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;private CameraManager mCameraManager;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;private String mCameraId;</p>			</td>		</tr>	</tbody></table><p>&nbsp;</p><ul>	<li>Now inside&nbsp;<strong>onCreate()</strong>&nbsp;we will get the&nbsp;<strong>CameraManager</strong>&nbsp;and&nbsp;<strong>Camera Id.</strong></li></ul><p>&nbsp;</p><p>Java</p><p><textarea></textarea></p><table>	<tbody>		<tr>			<td>			<p>1</p>			<p>2</p>			<p>3</p>			<p>4</p>			<p>5</p>			<p>6</p>			<p>7</p>			<p>8</p>			<p>9</p>			<p>10</p>			<p>11</p>			<p>12</p>			<p>13</p>			<p>14</p>			<p>15</p>			<p>16</p>			<p>17</p>			<p>18</p>			<p>19</p>			<p>20</p>			<p>21</p>			<p>22</p>			<p>23</p>			<p>24</p>			<p>25</p>			<p>26</p>			<p>27</p>			<p>28</p>			<p>29</p>			<p>30</p>			</td>			<td>			<p>&nbsp;&nbsp;&nbsp;&nbsp;@Override</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;protected void onCreate(Bundle savedInstanceState) {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;super.onCreate(savedInstanceState);</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;setContentView(R.layout.activity_main);</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;boolean isFlashAvailable = getApplicationContext().getPackageManager()</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if (!isFlashAvailable) {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;showNoFlashError();</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;//getting the camera manager and camera id</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mCameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;try {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mCameraId = mCameraManager.getCameraIdList()[0];</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;} catch (CameraAccessException e) {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e.printStackTrace();</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;toggleButton = findViewById(R.id.toggleButton);</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@Override</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;//we will call this method to switch the flash</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;switchFlashLight(isChecked);</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;});</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;}</p>			</td>		</tr>	</tbody></table><p>&nbsp;</p><ul>	<li>This time you can also see that inside the ToggleButton Listener we are calling a method named&nbsp;<strong>switchFlashLight()</strong>&nbsp;and we are passing the&nbsp;<strong>isChecked</strong>&nbsp;boolean variable to this method which tells the current state of the Toggle Button, whether it is On or Off.</li>	<li>This method&nbsp;<strong>switchFlashLight()</strong>&nbsp;will do the actual task so let&rsquo;s define it.</li></ul><h2>Turn on Flashlight Android</h2><ul>	<li>The code is very simple only a single line as we already defined the required objects.</li></ul><p>&nbsp;</p><p>Java</p><p><textarea></textarea></p><table>	<tbody>		<tr>			<td>			<p>1</p>			<p>2</p>			<p>3</p>			<p>4</p>			<p>5</p>			<p>6</p>			<p>7</p>			</td>			<td>			<p>&nbsp;&nbsp;&nbsp;&nbsp;public void switchFlashLight(boolean status) {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;try {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mCameraManager.setTorchMode(mCameraId, status);</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;} catch (CameraAccessException e) {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e.printStackTrace();</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;}</p>			</td>		</tr>	</tbody></table><p>&nbsp;</p><ul>	<li>Now you can try running your application.</li>	<li>If you are having any confusions, then here is the full code of our&nbsp;<strong>MainActivity.java</strong>.</li></ul><p>&nbsp;</p><p>MainActivity.java</p><p>Java</p><p><textarea></textarea></p><table>	<tbody>		<tr>			<td>			<p>1</p>			<p>2</p>			<p>3</p>			<p>4</p>			<p>5</p>			<p>6</p>			<p>7</p>			<p>8</p>			<p>9</p>			<p>10</p>			<p>11</p>			<p>12</p>			<p>13</p>			<p>14</p>			<p>15</p>			<p>16</p>			<p>17</p>			<p>18</p>			<p>19</p>			<p>20</p>			<p>21</p>			<p>22</p>			<p>23</p>			<p>24</p>			<p>25</p>			<p>26</p>			<p>27</p>			<p>28</p>			<p>29</p>			<p>30</p>			<p>31</p>			<p>32</p>			<p>33</p>			<p>34</p>			<p>35</p>			<p>36</p>			<p>37</p>			<p>38</p>			<p>39</p>			<p>40</p>			<p>41</p>			<p>42</p>			<p>43</p>			<p>44</p>			<p>45</p>			<p>46</p>			<p>47</p>			<p>48</p>			<p>49</p>			<p>50</p>			<p>51</p>			<p>52</p>			<p>53</p>			<p>54</p>			<p>55</p>			<p>56</p>			<p>57</p>			<p>58</p>			<p>59</p>			<p>60</p>			<p>61</p>			<p>62</p>			<p>63</p>			<p>64</p>			<p>65</p>			<p>66</p>			<p>67</p>			<p>68</p>			<p>69</p>			<p>70</p>			<p>71</p>			<p>72</p>			</td>			<td>			<p>package net.simplifiedcoding.mytorch;</p>			<p>&nbsp;</p>			<p>import android.content.Context;</p>			<p>import android.content.DialogInterface;</p>			<p>import android.content.pm.PackageManager;</p>			<p>import android.hardware.camera2.CameraAccessException;</p>			<p>import android.hardware.camera2.CameraManager;</p>			<p>import android.os.Bundle;</p>			<p>import android.support.v7.app.AlertDialog;</p>			<p>import android.support.v7.app.AppCompatActivity;</p>			<p>import android.widget.CompoundButton;</p>			<p>import android.widget.ToggleButton;</p>			<p>&nbsp;</p>			<p>public class MainActivity extends AppCompatActivity {</p>			<p>&nbsp;</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;private CameraManager mCameraManager;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;private String mCameraId;</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;private ToggleButton toggleButton;</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;@Override</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;protected void onCreate(Bundle savedInstanceState) {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;super.onCreate(savedInstanceState);</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;setContentView(R.layout.activity_main);</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;boolean isFlashAvailable = getApplicationContext().getPackageManager()</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;if (!isFlashAvailable) {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;showNoFlashError();</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}</p>			<p>&nbsp;</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mCameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;try {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mCameraId = mCameraManager.getCameraIdList()[0];</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;} catch (CameraAccessException e) {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e.printStackTrace();</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;toggleButton = findViewById(R.id.toggleButton);</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;@Override</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;switchFlashLight(isChecked);</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;});</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;}</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;public void showNoFlashError() {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AlertDialog alert = new AlertDialog.Builder(this)</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;.create();</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert.setTitle(&quot;Oops!&quot;);</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert.setMessage(&quot;Flash not available in this device...&quot;);</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert.setButton(DialogInterface.BUTTON_POSITIVE, &quot;OK&quot;, new DialogInterface.OnClickListener() {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;public void onClick(DialogInterface dialog, int which) {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;finish();</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;});</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;alert.show();</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;}</p>			<p>&nbsp;</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;public void switchFlashLight(boolean status) {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;try {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mCameraManager.setTorchMode(mCameraId, status);</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;} catch (CameraAccessException e) {</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;e.printStackTrace();</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;}</p>			<p>&nbsp;&nbsp;&nbsp;&nbsp;}</p>			<p>}</p>			</td>		</tr>	</tbody></table><p>So that is all for this Turn On Flashlight Android tutorial guys. I hope you found it helpful. If you have any query, just leave that in the comment section below. And don&rsquo;t forget to share this post with android newbies. Thank You ðŸ™‚</p>', '', '<p>Hey everyone, this is a tutorial for the newbies, the people just started getting there hands dirty on android coding. I thought I would post some small apps tutorial for you all, so that you can enjoy the learning. Today, in this post we will learn how to Turn on Flashlight in Android using Code. As the title says&nbsp;<strong>&ldquo;Turn on Flashlight Android &ndash; Building a Torch App&rdquo;,&nbsp;</strong>so we are going to build a simple torch application today.</p>', 1, '2019-05-24 16:45:45', '2019-05-24 16:45:45');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `comment` text NOT NULL,
  `rating` int(2) NOT NULL DEFAULT '0',
  `user_type` int(2) NOT NULL,
  `status` int(2) NOT NULL DEFAULT '1',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `name`, `email`, `user_id`, `comment`, `rating`, `user_type`, `status`, `created_date`) VALUES
(5, 'Mahalingam', 'mahalingam.b@gmail.com', 6, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam aliqua. Ut enim ad minim dolore magna aliqua. Ut enim ad minim veniam aliqua. Ut enim ad minim.', 4, 1, 2, '2018-09-25 08:28:22'),
(6, 'Pankaj Negi', 'mahalingam.b@gmail.com', 6, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam aliqua.', 5, 2, 2, '2018-09-25 08:32:04'),
(14, 'Shubham Uniyal', 'mahalingam.b@gmail.com', 6, 'Very Best Tutorials', 5, 2, 1, '2018-09-27 22:56:28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exam_time`
--
ALTER TABLE `exam_time`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `institute`
--
ALTER TABLE `institute`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_price`
--
ALTER TABLE `page_price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question`
--
ALTER TABLE `question`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `question_set`
--
ALTER TABLE `question_set`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_category`
--
ALTER TABLE `sub_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `exam_time`
--
ALTER TABLE `exam_time`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `institute`
--
ALTER TABLE `institute`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `page_price`
--
ALTER TABLE `page_price`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `question`
--
ALTER TABLE `question`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `question_set`
--
ALTER TABLE `question_set`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `sub_category`
--
ALTER TABLE `sub_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

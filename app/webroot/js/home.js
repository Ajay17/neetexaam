$(document).ready(function () {
    var institute_contactName,institute_email,institute_mobile,institute_password,institute_confirmPassword
    ,institute_address,institute_city,institute_website,institute_instituteName,institute_phone;
    $(".mobileNumber").keypress(function (e) {
      if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
               return false;
        }
        });

    $('#card2_more_data_more_link').on("click", function () {
        $('#card2_more_data').show("fast");
        $('#card2_more_data_more_link').hide("fast");
    });

    $('#card2_more_data_less_link').on("click", function () {
        $('#card2_more_data').hide("fast");
        $('#card2_more_data_more_link').show("fast");
    });


    $('#card3_more_data_more_link').on("click", function () {
        $('#card3_more_data').show("fast");
        $('#card3_more_data_more_link').hide("fast");
    });

    $('#card3_more_data_less_link').on("click", function () {
        $('#card3_more_data').hide("fast");
        $('#card3_more_data_more_link').show("fast");
    });

    $('#card4_more_data_more_link').on("click", function () {
        $('#card4_more_data').show("fast");
        $('#card4_more_data_more_link').hide("fast");
    });

    $('#card4_more_data_less_link').on("click", function () {
        $('#card4_more_data').hide("fast");
        $('#card4_more_data_more_link').show("fast");
    });

    $('#card5_more_data_more_link').on("click", function () {
        $('#card5_more_data').show("fast");
        $('#card5_more_data_more_link').hide("fast");
    });

    $('#card5_more_data_less_link').on("click", function () {
        $('#card5_more_data').hide("fast");
        $('#card5_more_data_more_link').show("fast");
    });

    $('#card6_more_data_more_link').on("click", function () {
        $('#card6_more_data').show("fast");
        $('#card6_more_data_more_link').hide("fast");
    });

    $('#card6_more_data_less_link').on("click", function () {
        $('#card6_more_data').hide("fast");
        $('#card6_more_data_more_link').show("fast");
    });

   
  $(".links").click(function(){
    $(".links").removeClass("active")
    var hash = $(this).attr("href");
   
    $("a[href='"+hash+"']").addClass("active")
    $('html, body').animate({
      scrollTop: $(hash).offset().top -$('header').height()
    }, 2000);
   
  })


  $('#student_resgister_link').click(function(){
    $('#registrationModal').modal('show')
  });
  
  $('#institute_resgister_link').click(function(){
    
    $('#institue_registrationModal').modal('show')
  });


  $('.close').click(function(){
    $('input').val('');
    $('.error_msg').hide();
    $('success_msg').hide();
    $('#institue_registrationModal').modal('hide')
  });

  $('.closebtn').click(function(){
    $('input').val('');
    $('.error_msg').hide();
    $('success_msg').hide();
    $('#institue_registrationModal').modal('hide')
  });
  
  
  
  $('#login_link').click(function(){
    $('#loginModal').modal('show');
    });

  
});




function instituteRegistration(url){
  institute_contactName =$("#instituteContactName").val();
  institute_email =$("#instituteEmailID").val();
  institute_mobile =$("#instituteMobileNumber").val();
  institute_password =$("#institutePassword").val();
  institute_confirmPassword =$("#instituteConfirmPassword").val();
  institute_address =$("#instituteAddress").val();
  institute_city =$("#instituteCity").val();
  institute_website =$("#instituteWebsite").val();
  institute_instituteName =$("#instituteName").val();
  institute_phone =$("#institutePhone").val();
 
  
  if(instituteRegistrationvalidation()){
    $('.btn_click').prop('disabled', true);
        $('.loader').show()
        var data={}
        data['contact_name'] =institute_contactName;
        data['email'] =institute_email;
        data['mobile'] =institute_mobile;
        data['password'] =institute_password;
        data['address'] =institute_address;
        data['city'] =institute_city;
        data['website'] =institute_website;
        data['institute_name'] =institute_instituteName;
        data['phone'] =institute_phone;
        console.log(data)
        
        $.ajax({
            url:url,
            type:'POST',
            data :{'data':data},
            success:function(data){
                $('.loader').hide()
                $('.btn_click').prop('disabled', false);
               
                if(data == 1){
                $(".success_msg").show();
                setTimeout(function(){ 
                    $('#institue_registrationModal').modal('hide');
                    $(".success_msg").hide();
                     $('input').val('');
                 }, 1000);
                }else{
                }
            },
            error: function (errorthrown) {
              $('.loader').hide()
              $('.btn_click').prop('disabled', false);
              $('input').val('');
              console.log(errorthrown)
            }
        });

    }
}


    function studentRegistration(url){
     
        institute_contactName =$("#studentContactName").val();
        institute_email =$("#studentEmailID").val();
        institute_mobile =$("#studentMobileNumber").val();
        institute_password =$("#studentPassword").val();
        institute_confirmPassword =$("#studentConfirmPassword").val();
      
        if(studentRegistrationvalidation()){
          $('.btn_click').prop('disabled', true);
              $('.loader').show()
              var data={}
              data['contact_name'] =institute_contactName;
              data['email'] =institute_email;
              data['mobile'] =institute_mobile;
              data['password'] =institute_password;
              console.log(data)
             
              $.ajax({
                  url:url,
                  type:'POST',
                  data :{'data':data},
                  success:function(data){
                      $('.loader').hide()
                      $('.btn_click').prop('disabled', false);
                    
                      if(data == 1){
                      $(".success_msg").show();
                      setTimeout(function(){ $('#registrationModal').modal('hide');
                          $('input').val('');
                         $(".success_msg").hide();
                       }, 1000);
                      }else{
                      }
                  },
                  error: function (errorthrown) {
                    $('.loader').hide()
                    $('.btn_click').prop('disabled', false);
                    $('input').val('');
                    console.log(errorthrown)
                  }
              });
      
          }
      }


        function login(url , student_url , institute_url){

          institute_email =$("#loginEmailID").val();
        institute_password =$("#loginPassword").val();
        type = $("input[name='user_type']:checked").val();


         if(loginValidation()){
          $('.btn_click').prop('disabled', true);
              $('.loader').show()
              var data={}
             
              data['email'] =institute_email;
              data['password'] =institute_password;
         data['type'] =type;
              $.ajax({
                  url:url,
                  type:'POST',
                  data :{'data':data},
                  success:function(data){
                    console.log(data)

                      $('.loader').hide()
                      $('.btn_click').prop('disabled', false);

                      if(data.trim() != 'invalid_user'){
              
                         window.location.href=data
                      }
                 
                      
                      else{
                        console.log('error')
$('#loginerror').show()
                      }
        

                    
                  },
                  error: function (errorthrown) {
                    $('.loader').hide()
                    $('.btn_click').prop('disabled', false);
                    $('input').val('');
                    console.log(errorthrown)
                  }
              });
      
          }
        }

        function loginValidation(){
           var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i
          if(!institute_email){    
               $("#loginerroremail").text("field cannot be blank");
                $("#loginerroremail").show();
                return false;
                }else{
                $("#loginerroremail").hide();
                if(!pattern.test(institute_email)){
                      $("#loginerroremail").text("Invalid Email ID");
                $("#loginerroremail").show();
                        return false;
                }else{
                        $("#loginerroremail").hide();
    
                }
            }


             if(!institute_password){
                $("#loginPassword").text("field cannot be blank");
                $("#loginerrorpassword").show();
                return false;
            }else{
                 $("#loginerrorpassword").hide();
            }

            return true;
        }

      function studentRegistrationvalidation(){
            var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i
            if(!institute_contactName){
               $("#errorname").text("field cannot be blank");
                $("#errorname").show();
                return false;
            }else{
                $("#errorname").hide();
            }
            if(!institute_email){    
               $("#erroremail").text("field cannot be blank");
                $("#erroremail").show();
                return false;
                }else{
                $("#erroremail").hide();
                if(!pattern.test(institute_email)){
                      $("#erroremail").text("Invalid Email ID");
                $("#erroremail").show();
                        return false;
                }else{
                        $("#erroremail").hide();
    
                }
            }

            if(!institute_mobile){
               
                 $("#errormobile").text("field cannot be blank");
                  $("#errormobile").show();
                return false;
            }else
            {
                 $("#errormobile").hide();
                if(institute_mobile.length != 10){
                    $("#errormobile").text("Mobile Number Should be 10 Digits");
                       $("#errormobile").show();
                    return false;
                }else
                {
                 $("#errormobile").hide();
                }
    
            }
           
    
            if(!institute_password){
                $("#errorpassword").text("field cannot be blank");
                $("#errorpassword").show();
                return false;
            }else{
                 $("#errorpassword").hide();
            }
    
            if(!institute_confirmPassword){
                $("#error_confirmpassword").text("field cannot be blank");
                $("#error_confirmpassword").show();
                return false;
            }else{
                $(".error_confirmpassword").hide();
            }
        
            if(institute_password != institute_confirmPassword){
                $("#error_confirmpassword").text("Password Mismatch !!!");
                $("#error_confirmpassword").show();
                return false;
            }else{
                $("#error_confirmpassword").hide();
    
            }            
        return true;
        }

      

    


         function instituteRegistrationvalidation(){
            var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i
            if(!institute_contactName){
               $("#ins_errorname").text("field cannot be blank");
                $("#ins_errorname").show();
                return false;
            }else{
                $("#ins_errorname").hide();
            }
            if(!institute_email){    
               $("#ins_erroremail").text("field cannot be blank");
                $("#ins_erroremail").show();
                return false;
                }else{
                $("#ins_erroremail").hide();
                if(!pattern.test(institute_email)){
                $("#ins_erroremail").text("Invalid Email ID");
                $("#ins_erroremail").show();
                        return false;
                }else{
                        $("#ins_erroremail").hide();
    
                }
            }
    
           
            if(!institute_password){
                $("#ins_errorpassword").text("field cannot be blank");
                $("#ins_errorpassword").show();
                return false;
            }else{
                 $("#ins_errorpassword").hide();
            }
    
            if(!institute_confirmPassword){
                $("#ins_error_confirmpassword").text("field cannot be blank");
                $("#ins_error_confirmpassword").show();
                return false;
            }else{
                $(".ins_error_confirmpassword").hide();
            }
        
            if(institute_password != institute_confirmPassword){
                $("#ins_error_confirmpassword").text("Password Mismatch !!!");
                $("#ins_error_confirmpassword").show();
                return false;
            }else{
                $("#ins_error_confirmpassword").hide();
    
            }


             if(!institute_mobile){
               
                 $("#ins_errormobile").text("field cannot be blank");
                  $("#ins_errormobile").show();
                return false;
            }else
            {
                 $("#ins_errormobile").hide();
                if(institute_mobile.length != 10){
                    $("#ins_errormobile").text("Mobile Number Should be 10 Digits");
                       $("#ins_errormobile").show();
                    return false;
                }else
                {
                 $("#ins_errormobile").hide();
                }
    
            }


            if(!institute_phone){
               
                 $("#ins_errorphone").text("field cannot be blank");
                  $("#ins_errorphone").show();
                return false;
            }else
            {
                 $("#ins_errorphone").hide();
            }

            if(!institute_address){               
                 $("#ins_erroraddress").text("field cannot be blank");
                  $("#ins_erroraddress").show();
                return false;
            }else
            {
                 $("#ins_erroraddress").hide();
            }

             if(!institute_city){
                 $("#ins_errorcity").text("field cannot be blank");
                  $("#ins_errorcity").show();
                return false;
            }else
            {
                 $("#ins_errorcity").hide();
            }

             if(!institute_instituteName){
                 $("#instituteName").text("field cannot be blank");
                  $("#instituteName").show();
                return false;
            }else
            {
                 $("#instituteName").hide();
            }

             if(!institute_website){
                 $("#instituteWebsite").text("field cannot be blank");
                  $("#instituteWebsite").show();
                return false;
            }else
            {
                 $("#instituteWebsite").hide();
            }
    
        return true;
        }
      


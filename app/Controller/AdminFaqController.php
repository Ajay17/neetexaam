<?php
   App::uses('AdminController', 'Controller');
 
  class AdminFaqController  extends AdminController {

    var $layout="adminlayout";
    var $uses = array("Faq","ExamTime","PagePrice", "Testimonial","Topic");
    public $components = array('Paginator');

    function addFaq(){
            $this->set('message','');
            if($this->request->is('post')){
               $this->Faq->create();
               $data= $this->Faq->save($this->request->data);
               $this->set('message','Data Inserted !!!');
           }
           $all_faq = $this->Faq->getAllFaq();
           $this->set('all_faq',$all_faq);
    }

    public function updateFaq(){
          $this->autoRender=false;
          if ($this->request->is('ajax')) {
            $data=($_POST['data']);
            $this->Faq->set('id',$data['question_set_id']);
            $this->Faq->save($data);
          }
    }

    public function deleteFaq(){
          $this->autoRender=false;
          if ($this->request->is('ajax')) {
                $data=($_POST['data']);
                $this->Faq->delete($data);
          }
    }

    public function insertExamtimer(){
             $this->set('message','');
            if ($this->request->is('ajax')) {
                 $this->autoRender=false;
                 $data=($_POST['data']);
                 $this->ExamTime->set('id',1);
                 $this->ExamTime->set('time',$data);
                 $data= $this->ExamTime->save();
                 $this->set('message','Data Inserted !!!');
            }
    }

    public function pagePricing() {
            $this->set('message','');
            if($this->request->is('post')){
               $this->PagePrice->create();
               $data= $this->PagePrice->save($this->request->data);
               $this->set('message','Data Inserted successfully.');
            }
            $all_page_price = $this->PagePrice->getAllPagePrice();
            $this->set('all_page_price',$all_page_price);
    }

    public function testimonials(){
        $this->paginate = array(
                        'contain' => array('Testimonial'),
                        'limit' => 3,
                        'order' => array('id' => 'desc')
        );
        $testimonials = $this->paginate('Testimonial');
        $this->set('testimonials', $testimonials);
            
    }

        public function updatePagePrice(){
              $this->autoRender=false;
              if ($this->request->is('ajax')) {
                 $data=($_POST['data']);
                 $this->PagePrice->set('id',$data['page_price_id']);
                 $this->PagePrice->save($data);
                }
        }

        public function deletePagePrice(){
            $this->autoRender=false;
          if ($this->request->is('ajax')) {
                $data=($_POST['data']);
                $this->PagePrice->delete($data);
            }
        }

        public function topic(){
         
        }
        public function saveTopic(){
          $this->autoRrender = false;
          $this->set('message','');
          if($this->request->is('post')){
            $this->Topic->create();
            $this->Topic->set('category_id',1);
            $this->Topic->set('title',$this->request->data['topic_title']);
            $this->Topic->set('subtitle',$this->request->data['topic_subtitle']);
            $this->Topic->set('description',$this->request->data['topic_description']);
            $this->Topic->set('youtube_link',$this->request->data['topic_youtube_link']);
            $this->Topic->set('content',$this->request->data['topic_main_content']);
            $this->Topic->set('admin_id',1);
            $this->Topic->set('created_at',date('Y-m-d H:i:s'));
            $this->Topic->set('modified_at',date('Y-m-d H:i:s'));
            if($this->Topic->save()){
             $this->redirect(array("controller" => "adminFaq", 
                      "action" => "topic"));

            }
          }
        }
       //get all topic json 
        public function allTopicJson(){
          $this->autoRender = false;
          $all_topic = $this->Topic->getAllTopic();
          //echo json_encode($all_topic, JSON_HEX_QUOT | JSON_HEX_TAG);
          echo htmlentities (print_r (json_encode($all_topic), true));  

        }
}
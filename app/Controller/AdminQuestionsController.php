                                                                                                                                                                                                                                                                      <?php
  /**
   * Created by Soarlogic Information Technologies Pvt. Ltd.
   * Developer Name: Ajay Mandrawal
   * Date: September 26 , 2017
   */
   App::uses('AdminController', 'Controller');

  /**
   * Admin Controller
   *
   * Add your admin actions within this controller class
   */
  class AdminQuestionsController  extends AdminController {

      var $uses = array("QuestionSet","Question");
          var $layout="adminlayout";
      /**
       *  create_userAction for create user .
       */

       /**
          * ADMIN FUNCTIONS
       */


          public function createQuestion(){
            
            if (isset($this->request->data['is_active'])){
                
            }else{
               $this->request->data['is_active'] = true;
            }
            $DIR ='img/';
              $this->set('message','');
                   if($this->request->is('post')){
                    $this->Question->create();
                   $data= $this->Question->save($this->request->data);
                   $id = $data['Question']['id'];
                 
                   $folder = $DIR.$id;
                       for($i=0 ; $i< count($_FILES) ; $i++){
                        if(!empty($_FILES[$i]['name'])){
                          
                            $filename='';
                            $filename =   $_FILES[$i]['name'];
                           
                            $info = getimagesize( $_FILES[$i]["tmp_name"]);
                            $extension = image_type_to_extension($info[2]);
                           
                            $filename =$i. $extension;
                            $filepath = $DIR.$id.'/'.$filename;
                           
                            if (!file_exists($folder)) {
                                mkdir($folder, 0777, true);
                                  
                        
                                }
                                if (move_uploaded_file($_FILES[$i]["tmp_name"],  $filepath)) {
                                    if($i == 0){
                                        $this->Question->set('id',$id);
                                        $this->Question->set('question_image',$filepath);
                                        $this->Question->save();
                                    }else if($i == 1){
                                        $this->Question->set('id',$id);
                                        $this->Question->set('option1_image',$filepath);
                                        $this->Question->save();
                                    }else if($i == 2){
                                        $this->Question->set('id',$id);
                                        $this->Question->set('option2_image',$filepath);
                                        $this->Question->save();
                                    }
                                    else if($i == 3){
                                        $this->Question->set('id',$id);
                                        $this->Question->set('option3_image',$filepath);
                                        $this->Question->save();
                                    }
                                    else if($i == 4){
                                        $this->Question->set('id',$id);
                                        $this->Question->set('option4_image',$filepath);
                                        $this->Question->save();
                                    }
                                    else if($i==5){
                                        $this->Question->set('id',$id);
                                        $this->Question->set('explanation',$filepath);
                                        $this->Question->save();
                                    }
                                    }

                                  

                       }
                    
        
                       }  $this->set('message','Data Inserted !!!');
              }
              $all_question_sets = $this->QuestionSet->getAllQuestionSet();
              $all_questions = $this->Question->getAllQuestions();
              $this->set('all_questions' ,$all_questions);
              $this->set('all_question_sets',$all_question_sets);
            
              


          }


          public function removeImage(){
            $this->autoRender=false;
            if ($this->request->is('ajax')) {
            $data=($_POST['data']);
            
            
            $this->Question->set('id',$data['id']);
            $this->Question->set($data['field'],'');
            $this->Question->save($data);

  
             }

          }
         
        

          public function createQuestionSet(){
              //  $this->autoRender=false;
              $this->set('message','');
               
                if($this->request->is('post')){
    
                    $this->QuestionSet->create();
                    $data= $this->QuestionSet->save($this->request->data);
                    $this->set('message','Data Inserted !!!');
                }
                $all_question_sets = $this->QuestionSet->getAllQuestionSet();
                $this->set('all_question_sets',$all_question_sets);
              
    
            }

            public function updateQuestion(){
              $this->autoRender=false;
              if ($this->request->is('ajax')) {
              $data=($_POST['data']);
              $this->Question->set('id',$data['question_id']);
              $this->Question->save($data);

    
               }

            }

            public function deleteQuestion(){

               $this->autoRender=false;
              if ($this->request->is('ajax')) {
              $data=($_POST['data']);
             $this->Question->delete($data);
             

        }

            }


              public function updateQuestionSet(){
              $this->autoRender=false;
              if ($this->request->is('ajax')) {
              $data=($_POST['data']);
              $this->QuestionSet->set('id',$data['question_set_id']);
              $this->QuestionSet->save($data);

    
               }

            }

            public function deleteQuestionSet(){

               $this->autoRender=false;
              if ($this->request->is('ajax')) {
              $data=($_POST['data']);
             $this->QuestionSet->delete($data);
             

        }

            }



            public  function updateImage(){
                $DIR ='img/';
                $this->autoRender=false;
                if($this->request->is('ajax')){
                    $data=$_POST;
                     
                        $id =$_POST['id'];
                        $field =$_POST['field'];
                        $name =$_POST['name'];
                        $folder = $DIR.$id;
                


                        if(!empty($_FILES['files']['name'])){
                            
                              $filename='';
                              $filename =  $_FILES['files']['name'];
                             
                              $info = getimagesize( $_FILES['files']["tmp_name"]);
                              $extension = image_type_to_extension($info[2]);
                             
                              $filename =$name. $extension;
                              $filepath = $DIR.$id.'/'.$filename;
                             
                              if (!file_exists($folder)) {
                                  mkdir($folder, 0777, true);
                                    
                          
                                  }
                                  if (move_uploaded_file($_FILES['files']["tmp_name"],  $filepath)) {
                                     
                                          $this->Question->set('id',$id);
                                          $this->Question->set($field,$filepath);
                                         $data = $this->Question->save();

                                        echo '../app/webroot/'.$data['Question'][$field];
                                      
                                      }
  
                                    
  
                         }
                      
          
                }
              }
    
    

       

  }
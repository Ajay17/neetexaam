<?php
  
   App::uses('AdminController', 'Controller');
   App::uses('BlowfishPasswordHasher', 'Controller/Component/Auth');

 
  class AdminLoginController  extends AdminController {

    var $layout="admin_login_layout";


    public function beforeFilter() {
      parent::beforeFilter();
      $this->Auth->allow('adminLogin','getPassword');
  }


  public function getPassword(){
    $passwordHasher = new BlowfishPasswordHasher();
    $password =$passwordHasher->hash(
      12345
  );
  pr($password);die;
}
  
    public function adminLogin(){

     if($this->request->is('post')){
            pr($this->Auth->login());
      if ($this->Auth->login()) {
        return $this->redirect($this->Auth->redirectUrl());
      }else{
        
      }
     
      }
      

  }


  public function logout() {
    return $this->redirect($this->Auth->logout());
}

}
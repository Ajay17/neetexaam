<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {


    // public function beforeFilter() {
    //     parent::beforeFilter();
    //    $this->Session->read('student_login')) 
    //     $this->Session->read('student_login_id'))   
    // }

    public function authStudent(){
        $student_login_status = $this->Session->read('student_login');
        if($student_login_status == 'login'){
         $login_student_id =   $this->Session->read('student_login_id');
         $user_type = $this->Session->read('user_type');
         $this->set('user_type',$user_type);
         $this->set('login_student_id',$login_student_id);
        return true;
        }else{
            $this->redirect(array("controller" => "Homes",
            "action" => "home"));
        }
    }

    public function getLoginStudentId(){
        $login_student_id =   $this->Session->read('student_login_id') ;
        $this->set('login_student_id',$login_student_id);
        return $login_student_id;
    }

    public function getLoginData(){
        $login_student_id = $this->Session->read('student_login_id');
        $user_type = $this->Session->read('user_type');
        $data = array();
        $data['Login']['student_login_id'] = $login_student_id;
        $data['Login']['user_type'] = $user_type;
        $this->set('loginData', $data);
        return $data;

    }
  
}

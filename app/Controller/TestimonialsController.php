<?php

App::uses('AppController', 'Controller');

class TestimonialsController extends AppController {

  var $uses = array("Student","Testimonial");

 public function add_testimonial(){
    $this->autoRender = false;
    if($this->request->is('post')) {
      $user_data = $this->Student->findById($this->request->data["user_id"]);
      //print_r($user_data);
      //$this->request->data['name'] = $user_data['Student']['contact_name'];
      $this->request->data['email'] = $user_data['Student']['email'];
      $this->request->data['status'] = 1; 

      //print_r($this->request->data);
      $this->loadModel('Testimonial');
      $this->Testimonial->create();
      if($data = $this->Testimonial->save($this->request->data)){
        return json_encode(array('status' => true, 'message' => "Record Save Successfully"));
      }else{
        return json_encode(array('status' => false, 'message' => "Record Not Save Successfully"));
      }
    }else {
      return json_encode(array('status' => false, 
      'message' => "Request is not Post. Please check your Request Method"));
    }
   }

   public function delete_testimonial(){
      $this->autoRender = false;
      if($this->request->is('post')) {
          if($isDelete = $this->Testimonial->delete($this->request->data('id')) ){
            return json_encode(array('status' => true, 'message' => "Record Deleted Successfully"));
          }else{
            return json_encode(array('status' => false, 'message' => "Record Not Deleted Successfully"));
          }
      }else {
        return json_encode(array('status' => false, 
        'message' => "Request is not Post. Please check your Request Method"));
      }
   }

   
   public function change_testimonial_status(){
    $this->autoRender = false;
    if($this->request->is('post')) {
      $this->Testimonial->id = $this->request->data('id');
      $data=array();
      $data['Testimonial']['status'] = 2;
      $this->Testimonial->set($data);
      if($data = $this->Testimonial->save()) {
        return json_encode(array('status' => true, 'message' => "Status Changed Successfully"));
      }else{
        return json_encode(array('status' => false, 'message' => "Status Not Changed Successfully"));
      }
    }else {
      return json_encode(array('status' => false, 
      'message' => "Request is not Post. Please check your Request Method"));
    }
   }
}
    <?php

    App::uses('AppController', 'Controller');

    class HomesController extends AppController
    {
        public $layout = 'homelayout';
        var $uses = array("Institute","Student" ,"ExamTime","Faq","Testimonial");

        public function home(){
          // $auth_user =$this->authStudent();
         
          $student_login_status = $this->Session->read('student_login');
          $this->set('auth_status',$student_login_status);
     
           $exam_time =   $this->ExamTime->getExamTime();
           $testimonials = $this->Testimonial->getTenTestimonials();
           $exam_time =$exam_time[0]['ExamTime']['time'];
           $this->set('exam_time',$exam_time);
           $date = new DateTime($exam_time);
           $exam_time_stamp = $date->getTimestamp();
           $all_faq = $this->Faq->getAllFaq();
           $this->set('all_faq',$all_faq);
           $this->set('exam_time_stamp',$exam_time_stamp.'000');
           $this->set('testimonial_datas', $testimonials);

           if($this->request->is('ajax')){
                $data=($_POST['data']);
                $user_type = $this->request->data['type'];
                $email = $this->request->data['email'];
                $password = $this->request->data['password'];

                 if($user_type == 'student') {
                    $result = $this->Student->studentLogin( $email ,$password); 
                     if(!empty($result)){
                    
                        echo '1';
                      }else{
                        echo '2';
                      }
                }else if($user_type == 'institute'){

                  $result = $this->Institute->intitueLogin( $email ,$password); 
                    if(!empty($result)){
                    
                        echo '1';
                      }else{
                        echo '2';
                      }
                      }

                 }

               }

 public function login()
        {
         
         $this->autoRender=false;
           if($this->request->is('ajax')){
            $data=($_POST['data']);
                $user_type = $this->request->data['type'];
                $email = $this->request->data['email'];
               $password = $this->request->data['password'];

                 if($user_type == 'student') {
                   
                      $result = $this->Student->studentLogin( $email ,$password); 
                   
                               
                     if(!empty($result)){
                     
                       $this->Session->write('student_login', 'login'); 
                       $this->Session->write('student_login_id', $result['Student']['id']);
                       $this->Session->write('user_type', 1);
                      
                      $student_url =Router::url(["controller" => "students", "action" => "student_dashboard"]);
                      echo $student_url;
                      }else{
                        echo 'invalid_user';
                      }
                }else if($user_type == 'institute'){

                  $result = $this->Institute->intitueLogin( $email ,$password); 
                    if(!empty($result)){
                    
                      $institute_url =Router::url(["controller" => "institutes", "action" => "institute_dashboard"]);
                      echo $institute_url;
                      }else{
                        echo 'invalid_user';
                      }
                      }

                 }

               }

        

        public function contact_us()
        {
            
        }

        public function instituteRegistration()
        {
            $this->autoRender=false;
            if ($this->request->is('ajax')) {

                $data=($_POST['data']);
              if($this->Institute->save($data)){
                  echo 1;
              }else{
                  echo 2;
              }
            }
            
        }

        public function studentRegistration()
        {
            $this->autoRender=false;
            if ($this->request->is('ajax')) {

                $data=($_POST['data']);
              if($this->Student->save($data)){
                  echo 1;
              }else{
                  echo 2;
              }
            }
            
        }


    }

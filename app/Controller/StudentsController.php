<?php

App::uses('AppController', 'Controller');

class StudentsController extends AppController
{
   
    public $layout = 'student_layout';
    var $uses = array("Student", "QuestionSet" ,"Question");
    public function student_dashboard(){
        if($this->authStudent()){
            $login_student_id = $this->getLoginStudentId(); 
            $student_profile =  $this->Student->getStudentProfile($login_student_id);
            $this->set('student_profile',$student_profile);
        }else{
          
        }
    }

   public function subscriptionPage(){
      if($this->authStudent()){
        $login_student_id = $this->getLoginStudentId(); 
        $student_profile =  $this->Student->getStudentProfile($login_student_id);
        $this->set('student_profile',$student_profile);
      }else{
      
      }
   }

  public function student_profile(){
      if($this->authStudent()){

        $login_student_id = $this->getLoginStudentId(); 
        $student_profile =  $this->Student->getStudentProfile($login_student_id);
        $this->set('student_profile',$student_profile);
      }else{
        
      }
  }

  public function logoutStudent(){
    $this->Session->write('student_login', 'logout'); 
    $this->Session->write('student_login_id','');
    $this->Session->write('user_type', '');
    $this->redirect(array("controller" => "homes",
    "action" => "home"));
  }

  public function testimonial(){
    if($this->authStudent()){
        $login_student_id = $this->getLoginStudentId();
        $loginData = $this->getLoginData(); 
        $student_profile =  $this->Student->getStudentProfile($login_student_id);
        $this->set('student_profile',$student_profile);
        $this->set('loginData', $loginData);
      }else{
        
      }
  }


  public function get_question_set(){
    if($this->authStudent()){
        $login_student_id = $this->getLoginStudentId();
        $student_profile =  $this->Student->getStudentProfile($login_student_id);
        $this->set('student_profile',$student_profile);
        $questionSets = $this->QuestionSet->getAllQuestionSetByPagination();
        $this->set("questionSets", $questionSets);
      }else{
        
      }
  }

  function student_test_page($id){
    if($this->authStudent()){
      $login_student_id = $this->getLoginStudentId(); 
      $student_profile =  $this->Student->getStudentProfile($login_student_id);
      $this->set('student_profile',$student_profile);
      $exam_question =  $this->Question->getAllQuestionsByfields($id);
      $this->set('exam_question',$exam_question);
    }else{
    
    }
  }


  public function get_question_set_by_id($id){
    $this->autoRender=false;
    if($this->authStudent()){
      $login_student_id = $this->getLoginStudentId();
      $student_profile =  $this->Student->getStudentProfile($login_student_id);
      $this->set('student_profile',$student_profile);
      echo "Question Set ID :- " .$id;
    }else{
      
    }
  }
  
    public function updateStudentProfile(){
      $this->autoRender=false;
      if($this->request->is('ajax')){
        $this->Student->set('id',$_POST['data']['id']);
        $this->Student->set('contact_name',$_POST['data']['studentProfileName']);
        $this->Student->set('email',$_POST['data']['studentProfileEmailId']);
        $this->Student->set('mobile',$_POST['data']['studentProfilePhone']);
        $this->Student->set('city',$_POST['data']['studentProfileCity']);
          if($this->Student->save()){
                echo 1;
          }else{
                echo 2;
          }
        }
    }







  public  function updateStudentProfilePic(){
    $DIR ='img/student_profile/';
    $this->autoRender=false;
    if($this->request->is('ajax')){
        $data=$_POST;
         
            $id =$_POST['id'];
           
            $folder = $DIR.$id;
    
          

            if(!empty($_FILES['files']['name'])){
                
                  $filename='';
                  $filename =  $_FILES['files']['name'];
                 
                  $info = getimagesize( $_FILES['files']["tmp_name"]);
                  $extension = image_type_to_extension($info[2]);
                 
                //  $filename =$filename. $extension;
                  $filepath = $DIR.$id.'/'.$filename;
                 
                  if (!file_exists($folder)) {
                      mkdir($folder, 0777, true);
                        
              
                      }
                      if (move_uploaded_file($_FILES['files']["tmp_name"],  $filepath)) {
                         
                              $this->Student->set('id',$id);
                              $this->Student->set('profile_url',$filepath);
                             $data = $this->Student->save();

                            echo '../app/webroot/'.$data['Student']['profile_url'];
                          
                          }

                        

             }
          

    }
  }

}

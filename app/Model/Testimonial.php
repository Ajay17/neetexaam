<?php

App::uses('AppModel', 'Model');

class Testimonial extends AppModel {
    var $name ='Testimonial';
    var $useTable = "testimonials";
    var $primaryKey  ="id";

    function getAllTestimonial(){
        $results = $this->find('all');
        return $results;
    }

    function getTenTestimonials(){
        //Testimonial.status 2 means Status is Approved by Admin
        //Testimonial.user_type 1 means Student and 2 means Institue
        $studentCond = array('AND' => array(
                        'Testimonial.status' => 2,
                        'Testimonial.user_type' => 1,
                    ));
        $instituteCond = array('AND' => array(
                        'Testimonial.status' => 2,
                        'Testimonial.user_type' => 2,
                    ));
        $fields = array('Testimonial.id', 'Testimonial.name',
                        'Testimonial.user_id', 'Testimonial.comment', 
                        'Testimonial.rating','Testimonial.user_type' );
        $order = array('Testimonial.created_date' => 'desc');
        $limit = 5;
        $studentResults = $this->find('all', array(
                    'fields' => $fields,
                    'conditions' => $studentCond,
                    'limit' => $limit,
                    'order' => $order                    
        ));

        $instituteResults = $this->find('all', array(
                    'fields' =>$fields,
                    'conditions' => $instituteCond,
                    'limit' => $limit,
                    'order' => $order                     
        ));
        $results = array_merge($studentResults,$instituteResults);
        $modifiedData = array();
        //echo count($result);die;
        for($i=0; $i< count($results); $i++){
            $index = floor($i/2);
            if($index == 0){
                $modifiedData[$index]['status'] = 'active';
            }else{
                $modifiedData[$index]['status'] = '';
            }
            $modifiedData[$index]['testimonials'][$i] = $results[$i];
        }

        //echo "<pre>";
        //print_r($modifiedData);die;
        return $modifiedData;
    }

 
}
?>
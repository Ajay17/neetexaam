<?php

App::uses('AppModel', 'Model');

class Topic extends AppModel {
    var $name ='Topic';
    var $useTable = "sub_category";
    var $primaryKey  ="id";

    function getAllTopic(){
        $results = $this->find('all');
        return $results;
    }
}
?>

<?php

App::uses('AppModel', 'Model');

class QuestionSet extends AppModel {
  var $name ='QuestionSet';
  var $useTable = "question_set";
  var $primaryKey  ="id";

    function getAllQuestionSet(){
       $results = $this->find('all');
       return $results;
    }


    function getAllQuestionSetByPagination(){
      $results = $this->find('all');
      $modifiedData = array();
        //echo count($result);die;
        for($i=0; $i< count($results); $i++){
            $index = floor($i/3);
            $modifiedData[$index]['question_sets'][$i] = $results[$i];
        }
      return $modifiedData;
   }

 
}
?>

<?php

App::uses('AppModel', 'Model');

class Student extends AppModel {

	var $name ='Student';
    var $useTable = "student";

      function studentLogin($email , $password){

        $result = $this->find('first',array(
            'conditions'=>array(
                'email'=>$email,
                'password'=>$password
            )
          
        ));
        return $result;
        }

        function findById($id){
            $result = $this->findAllById($id);
            return $result[0];
        }


        function getStudentProfile($id){
            $result = $this->find('first',array(
                'conditions'=>array(
                    'id'=>$id
                )
                ));
             return $result;
        }

   
}
?>

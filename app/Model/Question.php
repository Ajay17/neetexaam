<?php

App::uses('AppModel', 'Model');

class Question extends AppModel {

	var $name ='Question';
    var $useTable = "question";
     var $primaryKey  ="id";


     var $belongsTo = array(
		'QuestionSet' => array(
			'className' => 'QuestionSet',
			'conditions' => '',
			'order' => '',
			'limit' => '',
			'foreignKey' => 'question_set_id',
			'dependent' => true,
			'exclusive' => true,
			'finderQuery' => '',
			'fields' => array("id","set_name"),
			'offset' => '',
            'counterQuery' => ''),

        

	);

    function getAllQuestions(){
            $results = $this->find('all',array(
            		'order'=>'Question.id DESC'
            ));
            //	pr($results);die;
            return $results;
		}
		
		function getAllQuestionsByfields($question_set_id = -1){
			$fields=array('id','question','option1','option2','option3','option4',
			'question_image','option1_image','option2_image','option3_image',
		'option4_image','correct_answer');
            $results = $this->find('all',array(
				'fields'=>$fields,
				'order'=>'Question.id DESC',
				'conditions' => array('Question.question_set_id' => $question_set_id)
            ));
            //	pr($results);die;
            return $results;
		}
   
}
?>

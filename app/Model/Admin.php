<?php

App::uses('AppModel', 'Model');

class Admin extends AppModel {

	var $name ='Admin';
    var $useTable = "admin";

      
    public $validate = array(
        'username' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'A username is required'
            )
        ),
        'password' => array(
            'required' => array(
                'rule' => 'notBlank',
                'message' => 'A password is required'
            )
        )
    );
   
}
?>

 <!DOCTYPE html>
    <html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php echo $this->Html->charset(); ?>
        <title>
           Neet Exam
        </title>
    
        <?php
        echo $this->Html->meta('icon');
        echo $this->Html->css('bootstrap.min.css');
        echo $this->Html->css('simple-sidebar.css');
        echo $this->Html->script('jquery.min.js');
        echo $this->Html->script('bootstrap.min.js');
        echo $this->Html->css('font-awesome.min.css');
        echo $this->Html->script('stars.js');
        echo $this->Html->script('sweetalert2.all.min.js');
		    echo $this->Html->script('nprogress.js');
       ?>


          <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/js/gijgo.min.js" type="text/javascript"></script>
          <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.10/css/gijgo.min.css" rel="stylesheet" type="text/css" />


    </head>
    <body>
      <?php echo $this->element('adminHeader') ?>
        <div id="wrapper" class="offsetlarge toggled">
           <?php echo $this->element('adminSideMenu') ?>
           <?php echo $this->fetch('content'); ?>
        </div>


    </body>
    </html>

   

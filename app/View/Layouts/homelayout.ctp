 <!DOCTYPE html>
    <html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php echo $this->Html->charset(); ?>
        <title>
           Neet Exam
        </title>
    
        <?php
        echo $this->Html->meta('icon');
        echo $this->Html->css('bootstrap.min.css');
      //  echo $this->Html->css('font-awesome.min.css');
        echo $this->Html->css('animate.css');
         echo$this->Html->css('hover-min.css');
         echo$this->Html->css('subscribe_page.css');
       // echo $this->Html->css('ico.css');
        echo $this->Html->css('owl.carousel.css');
        echo $this->Html->css('owl.carousel.min.css');
        echo $this->Html->css('font-awesome.min.css');
        echo $this->Html->css('style.css');
        echo $this->Html->css('countdown.css');
        echo $this->Html->css('dashboard.css');
        echo $this->Html->css('graph.css');
        echo $this->Html->script('jquery.min.js');
        echo $this->Html->script('bootstrap.min.js');
        echo $this->Html->script('owl.carousel.js');
        echo $this->Html->script('wow.min.js');
        echo $this->Html->script('home.js');
        echo $this->Html->script('countdown.js');
        echo $this->Html->script('jquery.flot.min.js');
        echo $this->Html->script('stars.js');
        echo $this->Html->script('sweetalert2.all.min.js');
		    echo $this->Html->script('nprogress.js');
        ?>
    </head>
    <body>
      <div>
          <?php echo $this->fetch('content'); ?>
      </div>
    </body>
</html>
<nav class="navbar navbar-expand-lg fixed navbar-inverse bg-light">
<a class="navbar-brand" href="#"><i class="fa fa-bars" aria-hidden="true"></i></a>
 
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse" id="navbarSupportedContent">
<ul class="navbar-nav mr-auto ">
<li class="nav-item dropdown pull-right">
<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
 <?php echo $student_profile['Student']['contact_name'] ?>
</a>

<div class="dropdown-menu" aria-labelledby="navbarDropdown">
<?php $home_page = Router::url(['controller' => 'students' , 'action' => 'student_dashboard']) ?>
<a class="dropdown-item" href='<?php echo $home_page ?>'>Home</a>
<?php $student_profile = Router::url(['controller' => 'students' , 'action' => 'student_profile']) ?>
<a class="dropdown-item" href='<?php echo $student_profile ?>'>Profile</a>
<?php $subscription_page = Router::url(['controller' => 'students' , 'action' => 'subscriptionPage']) ?>

<a class="dropdown-item" href='<?php echo $subscription_page ?>'>Subscriptions</a>
<?php $get_question_set = Router::url(['controller' => 'students' , 'action' => 'get_question_set']) ?>
<a class="dropdown-item" href="<?= $get_question_set ?>">NEET Exams</a>
<a class="dropdown-item" href="#">NEET Previous Year Exams</a>
<?php $testimonial_student = Router::url(['controller' => 'students' , 'action' => 'testimonial']) ?>
<a class="dropdown-item" href="<?php echo $testimonial_student ?>">Testimonial</a>
<div class="dropdown-divider"></div>
<?php $logout_student = Router::url(['controller' => 'students' , 'action' => 'logoutStudent']) ?>
<a class="dropdown-item" href='<?php echo $logout_student ?>'>Logout</a>
</div>
</li>
</div>
</nav>
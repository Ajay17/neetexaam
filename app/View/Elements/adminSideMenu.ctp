 <div id="sidebar-wrapper" class="offsetlg">
            <ul class="sidebar-nav">
                <li>
                <?php $url = Router::url(['controller' => 'AdminQuestions', 'action' => 'createQuestionSet'])?>
                <a href='<?php echo $url ?>'>Create Question Set</a>
                </li>
                <li>
                <?php $url = Router::url(['controller' => 'AdminQuestions', 'action' => 'createQuestion'])?>
                    <a href='<?php echo $url ?>'>Create Question</a>
                </li>
                <li>
                <?php $url = Router::url(['controller' => 'AdminFaq', 'action' => 'addFaq'])?>
                    <a href='<?php echo $url ?>'>Add FAQ</a>
                </li>
                <li>
                <?php $url = Router::url(['controller' => 'AdminFaq', 'action' => 'insertExamtimer'])?>
                    <a href='<?php echo $url ?>'>Timer</a>
                </li>
                <li>
                <?php $url = Router::url(['controller' => 'AdminFaq', 'action' => 'pagePricing'])?>
                    <a href='<?php echo $url ?>'>Page Pricing</a>
                </li>
                <li>
                    <?php $url = Router::url(['controller' => 'AdminFaq', 'action' => 'testimonials'])?>
                    <a href='<?php echo $url ?>'>Testimonials</a>
                </li>
                <li>
                    <?php $url = Router::url(['controller' => 'AdminFaq', 'action' => 'topic'])?>
                    <a href='<?php echo $url ?>'>Topic </a>
                </li>
                  <li>
                    <?php $url = Router::url(['controller' => 'AdminFaq', 'action' => 'allTopicJson'])?>
                    <a href='<?php echo $url ?>' target='_blank'>Topic Json </a>
                </li>

               
                
            </ul>
        </div>
<header>
  <div class="container">
      <div class="topNav">
        <ul class="navbar animated  miniNav">
        <li>  <a href="#" class="brand-logo"><img style="width:160px" src="<?php echo $this->webroot; ?>img/logo.svg" alt="logo"  /></a></li>
          <li class="active hvr-underline-from-center"><a href="#homeSection" class="links">home</a></li>
          <li class="hvr-underline-from-center"><a href="#aboutSection" class="links">about us</a></li>
          <li class="hvr-underline-from-center"><a class="links">NEET questions</a> </li>
          <li class="hvr-underline-from-center"> <a href="#contactSection" class="links"> contact </a></li>
          <li class="hvr-underline-from-center"><a href="#servicesSection" class="links"> services </a></li>
          
          
          <?php if($auth_status  == 'login'){ ?>
            <?php $url = Router::url(['controller' => 'Students', 'action' => 'student_dashboard'])?>
            <li class="hvr-underline-from-center"><a href='<?php echo $url ?>' class="links"> Dashboard </a></li>

         <?php }else{ ?>
          <li class="hvr-underline-from-center" id="login_link"><a  class="links"> Sign In </a></li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Sign Up
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a id="student_resgister_link" class="hvr-underline-from-center dropdown-item" >as student</a>
              <a id="institute_resgister_link" class="hvr-underline-from-center dropdown-item" >as institute </a>
            </div>
        </li>
          <?php } ?>
         

        </ul>
      </div>
  </div>
  <div class="mobHeader ">
    <div class="headerMini">
      <i class="fa fa-list-ul" aria-hidden="true" onclick="openNav()"><img style="width:96px ;right: 15px;position: absolute;" src="img/logo.png" alt="logo"></i>
      <ul class="animated sidenav mobNav" id="mySidenav" class="">
        <i  onclick="closeNav()" class="closebtn fa fa-times" id="togg"></i>
        <li> </li>
        <?php $home = Router::url(['controller' => 'homes', 'action' => 'home'])?>
        <li class="active hvr-underline-from-center"><a class="" href='<?php echo $home ?>'>home</a></li>
        <li class="hvr-underline-from-center"><a href="">about us</a></li>
        <li class="hvr-underline-from-center"><a href="">NEET questions</a> </li>
        <?php $contact_us = Router::url(['controller' => 'homes', 'action' => 'contact_us'])?>

        <li class="hvr-underline-from-center"><a class="" href='<?php echo $contact_us ?>'> contact </a></li>
        <li class="hvr-underline-from-center"><a> services </a></li>
        <li class="hvr-underline-from-center" id=""><a >s i g n u p</a></li>
      </ul>
    </div>
  </div>
</header>



<div id="loginModal" class="signUpForm modal " role="dialog">
<div class="modal-dialog" role="document">
<div class="modal-content">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
<div class="row">
<div class="col-md-4 leftBox">
<div class="signUpContent">
<h2 class="signUpHeading">Features of <br><span>I Face NEET Exam</span></h2>
<p class="signUpDisc">
<i class="fa fa-check-circle" aria-hidden="true"></i>NEET exam Previous Year questions Practice </p>
<p class="signUpDisc">
<i class="fa fa-check-circle" aria-hidden="true"></i>Previous Year NEET Exam questions as Mock Test
</p>
<p class="signUpDisc">
<i class="fa fa-check-circle" aria-hidden="true"></i>1000+ NEET Exam Question
</p>
<p class="signUpDisc">
<i class="fa fa-check-circle" aria-hidden="true"></i>50+ Mock Full Test with result analysis
</p>
<p class="signUpDisc">
<i class="fa fa-check-circle" aria-hidden="true"></i>Participate in scheduled All India Mock Test and get you rank and much more…
</p> 
</div>
</div>
<div class="col-md-8 rightBox">
<h2 class="text-center primeText">Signin to continue</h2>
<div class="container myForm signUpForm d-flex flex-column">

<div class="row">
<div class="col">
<div class="form-group">
<input type="text" id="loginEmailID" name="loginemailid" onfocusout="myFocusOut('loginerroremail' ,'loginemailid')" class="form-control" required>
<label class="form-control-placeholder" for="name">Email</label>
<span class="error" id="loginerroremail"><span class="fa fa-info-circle"></span> Error Message</span> 
</div>
</div>
</div> 

<div class="row">
<div class="col">
<div class="form-group">
<input type="password" id="loginPassword" name="loginpassword" onfocusout="myFocusOut('loginerrorpassword' ,'loginpassword')" class="form-control" required>
<label class="form-control-placeholder" for="name">Password</label>
<span class="error" id="loginerrorpassword"><span class="fa fa-info-circle"></span> Error Message</span> 
</div>
</div>
</div> 

<div class="form-group radio">
<label>Type of User</label><br>
<!-- Default unchecked -->
<div class="custom-controls">
<div class="custom-control custom-radio">
<input type="radio" class="custom-control-input" id="defaultUnchecked" name="user_type" value="student" checked>
<label class="custom-control-label" for="defaultUnchecked">Student</label>
</div>

<!-- Default checked -->
<div class="custom-control custom-radio">
<input type="radio" class="custom-control-input" id="defaultChecked" name="user_type" value="institute" >
<label class="custom-control-label" for="defaultChecked">Institute</label>

</div> 
</div> 

</div>
<div class="alert alert-danger "  style="display: none;" id="loginerror">Invalid EmailId or Password</div> 
<div class="modal-footer justify-content-center">
<span class="loader"></span>
<span class="success_msg">
<strong>Sign Up Done !!!</strong> 
</span>

<?php $institute_url =Router::url(["controller" => "institutes", "action" => "institute_dashboard"]);?>
<?php $student_url =Router::url(["controller" => "students", "action" => "student_dashboard"]);?>
<?php $url =Router::url(["controller" => "homes", "action" => "login"]);?>

<button class=" btn_click hvr-ripple-out primeBg caps btn" onclick="login('<?php echo $url ?>' , '<?php echo $student_url ?>' ,'<?php echo $institute_url ?>')">Sign In</button>



</div>
</div>
</div>
</div>


</div>

</div>
</div>
</div>






<div id="registrationModal" class="signUpForm modal " role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <div class="row">
          <div class="col-md-4 leftBox">
             <div class="signUpContent">
                <h2 class="signUpHeading">Features of  <br><span>I Face NEET Exam</span></h2>
                <p class="signUpDisc">
                  <i class="fa fa-check-circle" aria-hidden="true"></i>NEET exam Previous Year questions Practice </p>
                  <p class="signUpDisc">
                  <i class="fa fa-check-circle" aria-hidden="true"></i>Previous Year NEET Exam questions as Mock Test
                  </p>
                  <p class="signUpDisc">
                  <i class="fa fa-check-circle" aria-hidden="true"></i>1000+ NEET Exam Question
                  </p>
                  <p class="signUpDisc">
                  <i class="fa fa-check-circle" aria-hidden="true"></i>50+ Mock Full Test with result analysis
                  </p>
                  <p class="signUpDisc">
                  <i class="fa fa-check-circle" aria-hidden="true"></i>Participate in scheduled All India Mock Test and get you rank and much more…
                </p>               
              </div>
          </div>
         <div class="col-md-8 rightBox">
          <h2 class="text-center primeText">Signup to continue</h2>
           <div class="container myForm signUpForm">
            <div class="row">
              <div class="col">
              <div class="form-group">
                <input type="text" id="studentContactName" onfocusout="myFocusOut('errorname' ,'contact_name')" name="contact_name" class="form-control" required>
                <label class="form-control-placeholder" for="name">Contact Name</label>
                <span class="error" id="errorname"><span class="fa fa-info-circle"></span> Error Message</span>  
              </div>
              </div>
            </div>   
          
              <div class="row">
            <div class="col">
              <div class="form-group">
                <input type="text" id="studentEmailID" name="emailid" onfocusout="myFocusOut('erroremail' ,'emailid')" class="form-control" required>
                <label class="form-control-placeholder" for="name">Email</label>
                <span class="error" id="erroremail"><span class="fa fa-info-circle"></span> Error Message</span>  
              </div>
            </div>
          </div>  
          <div class="row">
            <div class="col">
              <div class="form-group">
                <input type="text" id="studentMobileNumber"  name="mobileNumber" onfocusout="myFocusOut('errormobile' ,'mobileNumber')" maxlength="10"  class="form-control mobileNumber" required>
                <label class="form-control-placeholder" for="name">Mobile</label>
                <span class="error" id="errormobile"><span class="fa fa-info-circle"></span> Error Message</span>  
              </div>
            </div>
          </div>  

          <div class="row">
            <div class="col">
           <div class="form-group">
                <input type="password" id="studentPassword" name="user_password" onfocusout="myFocusOut('errorpassword' ,'user_password')"  class="form-control" required>
                <label class="form-control-placeholder" for="name">Password</label>
                <span class="error" id="errorpassword"><span class="fa fa-info-circle"></span> Error Message</span>  
              </div>
            </div>
          </div>  

          <div class="row">
            <div class="col">
            <div class="form-group">
                <input type="password" id="studentConfirmPassword" name="user_confirm_password"  onfocusout="myFocusOut('error_confirmpassword' ,'user_confirm_password')" class="form-control" required>
                <label class="form-control-placeholder" >Confirm Password</label>
                 <span class="error" id="error_confirmpassword"><span class="fa fa-info-circle"></span> Error Message</span>  
              </div>
            </div>
          </div>  
        
          <div class="modal-footer justify-content-center">
              <span class="loader"></span>
              <span class="success_msg">
                <strong>Sign Up Done   !!!</strong> 
              </span>
              <?php $url =Router::url(["controller" => "homes", "action" => "studentRegistration"]);?>
              <button class=" btn_click  hvr-ripple-out primeBg caps btn" onclick="studentRegistration('<?php echo $url ?>')">Sign Up</button>
      </div>
      </div>
        </div>
       </div>

     
      </div>
    
    </div>
  </div>
</div>


<div id="institue_registrationModal" class="signUpForm modal " role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <div class="row">
          <div class="col-md-4 leftBox">
             <div class="signUpContent">
                <h2 class="signUpHeading">Features of  <br><span>I Face NEET Exam</span></h2>
                <p class="signUpDisc">
                  <i class="fa fa-check-circle" aria-hidden="true"></i>NEET exam Previous Year questions Practice </p>
                  <p class="signUpDisc">
                  <i class="fa fa-check-circle" aria-hidden="true"></i>Previous Year NEET Exam questions as Mock Test
                  </p>
                  <p class="signUpDisc">
                  <i class="fa fa-check-circle" aria-hidden="true"></i>1000+ NEET Exam Question
                  </p>
                  <p class="signUpDisc">
                  <i class="fa fa-check-circle" aria-hidden="true"></i>50+ Mock Full Test with result analysis
                  </p>
                  <p class="signUpDisc">
                  <i class="fa fa-check-circle" aria-hidden="true"></i>Participate in scheduled All India Mock Test and get you rank and much more…
                </p>               
              </div>
          </div>
         <div class="col-md-8 rightBox">
          <h2 class="text-center primeText">Signup to continue</h2>
           <div class="container myForm signUpForm">
            <div class="row">
              <div class="col-6">
              <div class="form-group">
                <input type="text" id="instituteContactName" onfocusout="myFocusOut('ins_errorname' ,'ins_contact_name')" name="ins_contact_name" class="form-control" required>
                <label class="form-control-placeholder" for="name">Contact Name</label>
                <span class="error" id="ins_errorname"><span class="fa fa-info-circle"></span> Error Message</span>  
              </div>
              </div>
              <div class="col-6">
              <div class="form-group">
                <input type="text" id="instituteEmailID" name="ins_emailid" onfocusout="myFocusOut('ins_erroremail' ,'ins_emailid')" class="form-control" required>
                <label class="form-control-placeholder" for="name">Email</label>
                <span class="error" id="ins_erroremail"><span class="fa fa-info-circle"></span> Error Message</span>  
              </div>
            </div>
              
            </div>   



            <div class="row">
            <div class="col-6">
           <div class="form-group">
                <input type="password" id="institutePassword" name="ins_password" onfocusout="myFocusOut('ins_errorpassword' ,'ins_password')"  class="form-control" required>
                <label class="form-control-placeholder" for="name">Password</label>
                <span class="error" id="ins_errorpassword"><span class="fa fa-info-circle"></span> Error Message</span>  
              </div>
            </div>
            <div class="col-6">
            <div class="form-group">
                <input type="password" id="instituteConfirmPassword" name="ins_confirm_password"  onfocusout="myFocusOut('ins_error_confirmpassword' ,'ins_confirm_password')" class="form-control" required>
                <label class="form-control-placeholder" >Confirm Password</label>
                 <span class="error" id="ins_error_confirmpassword"><span class="fa fa-info-circle"></span> Error Message</span>  
              </div>
            </div>
          </div>  

          <div class="row">
            <div class="col-6">
              <div class="form-group">
                <input type="text" id="instituteMobileNumber"  name="ins_mobileNumber" onfocusout="myFocusOut('ins_errormobile' ,'ins_mobileNumber')" maxlength="10"  class="form-control mobileNumber">
                <label class="form-control-placeholder" for="name">Mobile</label>
                <span class="error" id="ins_errormobile"><span class="fa fa-info-circle"></span> Error Message</span>  
              </div>
            </div>
            <div class="col-6">
              <div class="form-group">
                <input type="text" id="institutePhone" name="ins_phone" onfocusout="myFocusOut('ins_errorphone' ,'ins_phone')" class="form-control mobileNumber">
                <label class="form-control-placeholder" for="name">Phone Number</label>
                <span class="error" id="ins_errorphone"><span class="fa fa-info-circle"></span> Error Message</span>  
              </div>
            </div>
          </div>  

          
              <div class="row">
              <div class="col-6">
              <div class="form-group">
                <input type="text" id="instituteAddress" onfocusout="myFocusOut('ins_erroraddress' ,'ins_Address')" name="ins_Address" class="form-control" required>
                <label class="form-control-placeholder" for="name">Address</label>
                <span class="error" id="ins_erroraddress"><span class="fa fa-info-circle"></span> Error Message</span>  
              </div>
              </div>
            <div class="col-6">
              <div class="form-group">
                <input type="text" id="instituteCity" name="ins_city" onfocusout="myFocusOut('ins_errorcity' ,'ins_city')" class="form-control" required>
                <label class="form-control-placeholder" for="name">City</label>
                <span class="error" id="ins_errorcity"><span class="fa fa-info-circle"></span> Error Message</span>  
              </div>
            </div>
          </div>  
          <div class="row">
         <!--    <div class="col-6">
              <div class="form-group">
                <input type="text" id="instituteName"  name="ins_insname" onfocusout="myFocusOut('ins_errorinstitutename' ,'ins_insname')"  class="form-control">
                <label class="form-control-placeholder" for="name">Institute Name</label>
                <span class="error" id="ins_errorinstitutename"><span class="fa fa-info-circle"></span> Error Message</span>  
              </div>
            </div> -->

            <div class="col-6">
              <div class="form-group">
                <input type="text" id="instituteName" name="ins_insname" onfocusout="myFocusOut('ins_errorinstitutename' ,'ins_insname')" class="form-control" required>
                <label class="form-control-placeholder" for="name">Institute Name</label>
                <span class="error" id="ins_errorinstitutename"><span class="fa fa-info-circle"></span> Error Message</span>  
              </div>
            </div>
            <div class="col-6">
              <div class="form-group">
                <input type="text" id="instituteWebsite" name="ins_website" onfocusout="myFocusOut('ins_errorwebsite' ,'ins_website')" class="form-control" required>
                <label class="form-control-placeholder" for="name">Website</label>
                <span class="error" id="ins_errorwebsite"><span class="fa fa-info-circle"></span> Error Message</span>  
              </div>
            </div>
          </div>  

         

          <div class="row">
           
          </div>  
        
          <div class="modal-footer justify-content-center">
              <span class="loader"></span>
              <span class="success_msg">
                <strong>Sign Up Done   !!!</strong> 
              </span>
              <?php $url =Router::url(["controller" => "homes", "action" => "instituteRegistration"]);?>
              <button class=" btn_click  hvr-ripple-out primeBg caps btn" onclick="instituteRegistration('<?php echo $url ?>')">Sign Up</button>
      </div>
      </div>
        </div>
       </div>

     
      </div>
    
    </div>
  </div>
</div>

<script>


function myFocusOut(id ,name) {
var value =$("input[name="+name+"]").val();
 var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i

    if(!value){
      $(".error").hide();
      $("#"+id).text("field cannot be blank");
      $("#"+id).show();
     }else{

      $(".error").hide();
        if((name == 'emailid' || name == 'ins_emailid') &&  !pattern.test(value)){
        $("#"+id).text("Invalid Email ID");
        $("#"+id).show();
        }

        if((name == 'mobileNumber' || name == 'ins_mobileNumber') &&  value.length != 10){
        $("#"+id).text("Mobile Number Should be 10 Digits");
        $("#"+id).show();
        }

         if(name == 'user_confirm_password'){
              var pass =$("input[name = user_password]").val();
              if(value  != pass){
                $("#"+id).text("Password Mismatch !!!");
              $("#"+id).show();
              }
        
        }

        if(name == 'ins_confirm_password'){
              var pass =$("input[name = ins_password]").val();
              if(value  != pass){
                $("#"+id).text("Password Mismatch !!!");
              $("#"+id).show();
              }
        
        }
        
     }
 

}


</script>
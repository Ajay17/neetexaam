<!-- Footer Section -->
<section id="footerSection">
    <div class="footer paddingXl">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="footerLogo">
                        <img src="img/logo-new.png" alt="logo" width="180px" style="opacity:0">
                    </div>
                    <p class="paraTextFooter">I Face NEET Exam is a platform for the aspiring students who are preparing to succeed the NEET Exam to get admission into MBBS/BDS.  Our platform basically helps the aspiring students to  test their readiness and preparedness for appearing the NEET Exam.</p>
                    <ul class="socialIcons">
                        <li><a href="javascript:void(0)"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="javascript:void(0)"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <div class="footerLinks">
                        <p class="footerHeading">QUICK LINKS</p>
                        <ul class="footerLinkArea">
                            <li><a href="javascript:void(0)">Terms</a></li>           
                            <li><a href="javascript:void(0)">Refunds</a></li>            
                            <li><a href="javascript:void(0)">Services</a></li>                 
                                   
                            <li><a href="javascript:void(0)">Contact us</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="footerLinks">
                        <p class="footerHeading">NEWSLETTER</p>
                        <p class="paraTextFooter">Sign up for our mailing list to get latest
                        updates and offers</p>
                        <form>
                          <div class="form-group offset25">
                            <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Enter your email...">
                            <i class="fa fa-paper-plane-o" aria-hidden="true"></i>
                          </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="footerBottom">
            <div class="">
             
            </div>
        </div>
    </div>
</section>
<script>
    function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
        document.getElementById("main").style.left = "250px";
    }

    function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        document.getElementById("main").style.left= "0";
    }
    $('.count').each(function () {
    $(this).prop('Counter',0).animate({
        Counter: $(this).text()
    }, {
        duration: 4000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
        }
    });
    });

    $('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
    responsiveClass:true,
    responsive:{
        0:{
            items:1,
            nav:true
        },
        600:{
            items:2,
            nav:false
        },
        1000:{
            items:3,
            nav:true,
            loop:false
        }
    }
    })
    if($(window).width() > 993) {
        $(window).scroll(function() {
            if ($(this).scrollTop() > 1){  
                $('header').addClass("sticky");
                $('.headBar').css({"transform":"translateY(-100px)"});
                // $('.topNav').css({"transform":"translateY(-110px)"});
            }
            else{
                $('header').removeClass("sticky");
                $('.headBar').css({"transform":"translateY(0px)"});
                $('.topNav').css({"transform":"translateY(0px)"});
            }
        });
    }
    new WOW().init(); 
</script>

 <?php //pr($exam_question); ?>;
<div class="container" id="pricing" >
    <div class="row" style="margin-top: 35px; width: 100%">
        <div style="  background-color: #eee;width: 100%;border-radius: 5px; border: 1px solid lightgrey;text-align: right;">
            <div class="row">   
                <div class="col-md-7">
                <img style="width:160px" src="/neetexam/img/logo.svg" alt="logo">
                </div>
                <div class="col-md-5" style="margin: auto">
                <span id="current_index" style="padding: 5px;">Current index</span><span>/</span><span style="padding: 5px;" id="total_ques">Total Question</span>
                </div>
            </div>
        </div>

        <div class="priceList  "  style=" width: 100% ">
             <ul class="priceListing test" >
                <li>
                    <label  class="containerx test_ques_label ">  <div id="question" style="min-height: 25px;">Question</div></label>
                </li>
                <li >
                    <label  class="containerx test_label"  style=" padding-left: 50px !important ">
                        <div id="option1" style="min-height: 13px;">Option 1</div>
                        <input type="radio"  name="optradio" value="1"/>
                        <span class="checkmark"></span>
                    </label>
                </li>
                <li >
                    <label  class="containerx test_label"  style=" padding-left: 50px !important ">
                        <div id="option2" style="min-height: 13px;">Option 2</div>
                        <input type="radio"  name="optradio" value="2"/>
                        <span class="checkmark"></span>
                    </label>
                </li>
                <li >
                    <label  class="containerx test_label"  style=" padding-left: 50px !important ">
                        <div id="option3" style="min-height: 13px;">Option 3</div>
                        <input type="radio"  name="optradio" value="3"/>
                        <span class="checkmark"></span>
                    </label>
                </li>
                <li >
                    <label  class="containerx test_label"  style=" padding-left: 50px !important ">
                    <div id="option4" style="min-height: 13px;">Option 4</div>    
                    <input type="radio"  name="optradio" value="4"/>
                        <span class="checkmark"></span>
                    </label>
                </li>
               
            </ul>
        </div>
    </div>
    <div class="row" style="margin-top: 35px; width: 100%">
        <div class="col-md-6" >
           
            <button class="btn_click hvr-ripple-out caps btn nextbtn" onclick="preQuestion()">Previous</button>
        </div>
        <div class="col-md-6" style="text-align: right">
            <button class=" btn_click hvr-ripple-out nextbtn caps btn"  onclick="nextQuestion()"  >Next</button>
        </div>
    </div>
</div>


<script>
     var i=0
     var exam_data 
     var total_question =0
     var currunt_index =0
     var result_array
    $(document).ready(function() {
        i=0
         exam_data = <?php echo json_encode($exam_question); ?>;
         total_question = exam_data.length
         result_array=[total_question]
         $('#total_ques').text(total_question)
        getQuestion(i)
    })
  

  function getQuestion(i){
    
   
         $('#current_index').text(i+1)
    var index = i.toString()
    ques = exam_data[index]['Question'] ['question'] 
  
    option1 = exam_data[index]['Question'] ['option1']
    option2 = exam_data[index]['Question']['option2']
    option3 = exam_data[index]['Question'] ['option3']
    option4 =exam_data[index]['Question']['option4']
    $('#question').text(ques)
    $('#option1').text(option1)
    $('#option2').text(option2)
    $('#option3').text(option3)
    $('#option4').text(option4)
    $('input:radio[name=optradio]:checked').prop('checked', false)
  }
 

    
   
      function nextQuestion(){
    
     var selected_answer
     selected_answer =$('input[name=optradio]:checked').val()
     
          if(selected_answer != undefined){
          if(i+1<= total_question){
            result_array[i]=selected_answer
            console.log(result_array)
            if(i+1< total_question){
           i= i+1
           getQuestion(i)
            }
          }
        }else {
            alert('select one answer')
        }
      
      }

      function preQuestion(){
        if(i > 0){
        i= i-1
        getQuestion(i)
        value = result_array[i]
        $("input[name=optradio][value=" + value + "]").prop('checked', true);
        }
    }

    </script>
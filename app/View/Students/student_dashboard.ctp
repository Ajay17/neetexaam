
<div class="my-dashboard">
<div class="container-fluid">
<div class="row offset">
<div class="col-md-3">
<div class="box">
<p class="text-data">2</p>
<p class="text-heading">Active Test</p>
<span class="fa fa-file-text ico-large"></span>
<a class="more-info" href="javascript:void(0)">More info <span class="fa fa-arrow-right"></span></a>
</div>
</div>
<div class="col-md-3">
<div class="box">
<p class="text-data">20</p>
<p class="text-heading">Completed Test</p>
<span class="fa fa-check ico-large"></span>
<a class="more-info" href="javascript:void(0)">More info <span class="fa fa-arrow-right"></span></a>
</div>
</div>
<div class="col-md-3">
<div class="box">
<p class="text-heading large-font">Subscribe Package</p
<p class="text-heading">Full Pack<br/>
Valid Til : 20/7/2018</p>
<span class="fa fa-dollar ico-large"></span>
<?php $subscription_page = Router::url(['controller' => 'students' , 'action' => 'subscriptionPage']) ?>
<a class="more-info"  href='<?php echo $subscription_page ?>' >More info <span class="fa fa-arrow-right"></span></a>
</div>
</div>
<div class="col-md-3">
<div class="box">
<p class="text-heading large-font">Next Schedule Exam:</p
<p class="text-heading">Count Down<br/>
<span class="count-down">20:20:00</span>
<span class="fa fa-calendar ico-large"></span>
<a class="more-info" href="javascript:void(0)">More info <span class="fa fa-arrow-right"></span></a>
</div>
</div>
</div>
<div class="row">
<div class="col-md-6 offset">
<div id="accordion">
<div class="card">
<div class="card-header">
<a class="card-link" data-toggle="collapse" href="#collapseOne">
Exa, Attempts All India Mock Test
</a>
</div>
<div id="collapseOne" class="collapse show" data-parent="#accordion">
<div class="card-body">
<div id="graph-wrapper">
<div class="graph-info">
<a href="javascript:void(0)" class="visitors">2017 Exam result</a>
<a href="javascript:void(0)" class="returning">2018 Exam Result</a>

</div>

<div class="graph-container">
<div id="graph-lines"></div>
<div id="graph-bars"></div>
</div>
</div></div>
</div>
</div>
</div>
</div>
<div class="col-md-6 offset">
<div id="accordion">
<div class="card">
<div class="card-header">
<a class="card-link" data-toggle="collapse" href="#collapseOne0">
Result Overview of last All India Mock Test
</a>
</div>
<div id="collapseOne0" class="collapse show" data-parent="#accordion">
<div class="card-body">
<div class="d-flex justify-content-around">
<div class="subject d-flex">
<div class="subject-one"><span class="fa fa-bar-chart"></span> Physics Analysis</div>
<div class="subject-one"><span class="fa fa-bar-chart"></span> Chemestry Analysis</div>
<div class="subject-one"><span class="fa fa-bar-chart"></span> Biology Analysis</div>	
</div>	
</div>
<div class="d-flex justify-content-around offset result-data">	
<div class="c100 p12 small">
<span>12%</span>
<div class="slice">
<div class="bar"></div>
<div class="fill"></div>
</div>
</div>
<div class="c100 p12 small green">
<span>12%</span>
<div class="slice">
<div class="bar"></div>
<div class="fill"></div>
</div>
</div>
<div class="c100 p12 small orange">
<span>12%</span>
<div class="slice">
<div class="bar"></div>
<div class="fill"></div>
</div>
</div>
</div>

</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<script>
$(document).ready(function () {

// Graph Data ##############################################
var graphData = [{
// Visits
data: [ [6, 1300], [7, 1600], [8, 1900], [9, 2100], [10, 2500], [11, 2200], [12, 2000], [13, 1950], [14, 1900], [15, 2000] ],
color: '#71c73e'
}, {
// Returning Visits
data: [ [6, 500], [7, 600], [8, 550], [9, 600], [10, 800], [11, 900], [12, 800], [13, 850], [14, 830], [15, 1000] ],
color: '#77b7c5',
points: { radius: 4, fillColor: '#77b7c5' }
}
];

// Lines Graph #############################################
$.plot($('#graph-lines'), graphData, {
series: {
points: {
show: true,
radius: 5
},
lines: {
show: true
},
shadowSize: 0
},
grid: {
color: '#646464',
borderColor: 'transparent',
borderWidth: 20,
hoverable: true
},
xaxis: {
tickColor: 'transparent',
tickDecimals: 2
},
yaxis: {
tickSize: 1000
}
});

// Bars Graph ##############################################
$.plot($('#graph-bars'), graphData, {
series: {
bars: {
show: true,
barWidth: .9,
align: 'center'
},
shadowSize: 0
},
grid: {
color: '#646464',
borderColor: 'transparent',
borderWidth: 20,
hoverable: true
},
xaxis: {
tickColor: 'transparent',
tickDecimals: 2
},
yaxis: {
tickSize: 1000
}
});

// Graph Toggle ############################################
$('#graph-bars').hide();

$('#lines').on('click', function (e) {
$('#bars').removeClass('active');
$('#graph-bars').fadeOut();
$(this).addClass('active');
$('#graph-lines').fadeIn();
e.preventDefault();
});

$('#bars').on('click', function (e) {
$('#lines').removeClass('active');
$('#graph-lines').fadeOut();
$(this).addClass('active');
$('#graph-bars').fadeIn().removeClass('hidden');
e.preventDefault();
});

// Tooltip #################################################
function showTooltip(x, y, contents) {
$('<div id="tooltip">' + contents + '</div>').css({
top: y - 16,
left: x + 20
}).appendTo('body').fadeIn();
}

var previousPoint = null;

$('#graph-lines, #graph-bars').bind('plothover', function (event, pos, item) {
if (item) {
if (previousPoint != item.dataIndex) {
previousPoint = item.dataIndex;
$('#tooltip').remove();
var x = item.datapoint[0],
y = item.datapoint[1];
showTooltip(item.pageX, item.pageY, y + ' visitors at ' + x + '.00h');
}
} else {
$('#tooltip').remove();
previousPoint = null;
}
});

});
</script>
<div class="col-md-12">
          <h2 class="text-center primeText">Add Testimonial</h2>
           <div class="container myForm signUpForm">
            <div class="row">
                  <div class="offset-1 col-10">
                    <div class="form-group">
                        <input type="text" id="testimonialName" onfocusout="myFocusOut('ins_errorname' ,'ins_contact_name')" name="testimonial_name" class="form-control" required>
                        <label class="form-control-placeholder" for="name">Person Name</label>
                        <span class="error" id="ins_errorname"><span class="fa fa-info-circle"></span> Error Message</span>  
                    </div>
                  </div>
                  <div class="offset-1 col-10">
                    <div class="form-group">
                        <textarea type="text" rows="4" id="testimonialComment" name="ins_emailid" onfocusout="myFocusOut('ins_erroremail' ,'ins_emailid')" class="form-control" required></textarea>
                        <label class="form-control-placeholder" for="name">Comment</label>
                        <span class="error" id="ins_erroremail"><span class="fa fa-info-circle"></span> Error Message</span>  
                    </div>
                  </div>
                  <div class="offset-1 col-10">
                      <div class="form-group">
                          <!-- <label class="form-control-placeholder" for="name">Rating</label> -->
                          <div class='rating_testimonial' id='rating_testimonial'></div>
                          <span class="error" id="ins_erroremail"><span class="fa fa-info-circle"></span> Error Message</span>  
                          <input type="hidden" id="testimonialStar" name="testimonial_star" class="form-control">
                          <input type="hidden" id="user_id" name="user_id" value="<?= $loginData['Login']['student_login_id'] ?>" class="form-control">
                          <input type="hidden" id="user_type" name="user_type" value="<?= $loginData['Login']['user_type'] ?>" class="form-control">
                      </div>
                  </div>
                  <div class="offset-1 col-10">
                      <div class="form-group justify-content-center">
                          <!-- <div class="col-md-3 pull-right" style> -->
                            <button id="save_testimonial_btn" class="btn_click hvr-ripple-out primeBg caps btn" style="display: table !important;margin: auto;">Save Testimonial</button>
                          <!-- </div> -->
                      </div>  
                  </div>
            </div> 
        </div>
</div>            
<script type="text/javascript">
      $(document).ready(function() {
          $('.rating_testimonial').stars({
                    stars: 5,
                    click:function(i){
                      //alert("Star " + i + " selected.");
                      $("#testimonialStar").val(""+i);
                    }
          });
          $('#save_testimonial_btn').click(function() {
                   $personName = $("#testimonialName").val();
                   $comment = $("#testimonialComment").val();
                   $starValue = $("#testimonialStar").val(); 
                   $user_id = $("#user_id").val();
                   $user_type = $("#user_type").val();
                   if( isEmpty($personName) || isEmpty($comment) || isEmpty($starValue) || isEmpty($user_id) )  {
                        alert("Please fill all the Fields");
                        return false;
                   }
                   NProgress.start();
                    $.ajax({
                        type: "post",
                        data: {"user_id": $user_id, "name" : $personName, "comment": $comment , "rating" : $starValue, "user_type": $user_type},
                        dataType: "JSON",
                        url: '<?= Router::url(["controller" => "Testimonials", "action" => "add_testimonial"]); ?>',
                        success: function(result) {
                            NProgress.done();
                            //alert(result.message);
                            if(result.status == true){
                               // swal('Success', result.message, 'success');
                               swal({
                                    title: 'Success',
                                    text: result.message,
                                    type: 'success',
                                    confirmButtonColor: '#3085d6',
                                    confirmButtonText: 'Ok'
                                }).then((result) => {
                                    if (result.value) {
                                        var url =  '<?php echo $this->Html->url(array('controller' => 'Students',
                                                    'action' => 'student_dashboard')); ?>';
                                        window.location.href = url;
                                    }
                                });

                            }else{
                                swal('Error', result.message, 'error'); 
                            }
                            //custom alert
                            //$("#contact-us-form")[0].reset();
                        },
                        error: function(e){
                            NProgress.done();
                            alert(e);
                            //custom alert
                            //swal('Oops...', e, 'error');
                        }
                    });
          });
      });

      function isEmpty(value){
        	if(value !== null && value !== '') 
            return false;
        	else 
            return true;     
    	}
</script>
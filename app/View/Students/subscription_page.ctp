
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="container" id="pricing" style="margin-top:30px;margin-bottom:30px">
    <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 offsettop35">
                        <div class="priceList hvr-grow text-center bronze_package">
                            <div class="priceHeader">
                                <p class="priceTittle">Platinum <span class="subTitle">Package</span></p>
                                <span class="shape"></span>
                            </div>
                            <ul class="priceListing">
                                <li>
                                    <span class="topRow">
                                        <span class="cur"><i class="fa fa-rupee"></i></span>
                                        <span class="price">5000</span>
                                    </span>
                                    <p class="duration">(1 Year)</p>
                                </li>
                                <li>
                                    <span class="listings hvr-glow"><span class="fa icon-draw-check-mark"></span>Unlimited Access to All Exams </span>
                                </li>
                                <li>
                                    <span class="fa icon-draw-check-mark"></span>Franchise for Sale Website Uniquely designed
                                    for Franchise only
                                </li>
                                <li>
                                    <span class="fa icon-draw-check-mark"></span>Franchise for Sale Website Uniquely designed
                                    for Franchise only
                                </li>
                                <li>
                                    <a href="JavaScript:Void(0);" class="listingBtn platinum hvr-push" id="bronzeID">Checkout</a>
                                </li>
                                </ul>
                        </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 offsettop35">
                <div class="priceList hvr-grow text-center bronze_package">
                    <div class="priceHeader gold">
                        <p class="priceTittle">Gold <span class="subTitle">Package</span></p>
                        <span class="shape"></span>
                    </div>
                    <ul class="priceListing">
                        <li>
                            <span class="topRow">
                            <span class="cur"><i class="fa fa-rupee"></i></span>
                                <span class="price">4000</span>
                            </span>
                            <p class="duration">(6 Month)</p>
                        </li>
                        <li>
                            <span class="listings hvr-glow"><span class="fa icon-draw-check-mark"></span>Unlimited Access to All Exams </span>
                        </li>
                        <li>
                            <span class="fa icon-draw-check-mark"></span>Franchise for Sale Website Uniquely designed
                            for Franchise only
                        </li>
                        <li>
                            <span class="fa icon-draw-check-mark"></span>Franchise for Sale Website Uniquely designed
                            for Franchise only
                        </li>
                        <li>
                            <a href="JavaScript:Void(0);" class="listingBtn gold hvr-push" id="bronzeID">Checkout</a>
                        </li>
                        </ul>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 offsettop35">
                <div class="priceList hvr-grow text-center bronze_package">
                    <div class="priceHeader silver">
                        <p class="priceTittle">Silver <span class="subTitle">Package</span></p>
                        <span class="shape"></span>
                    </div>
                    <ul class="priceListing">
                        <li>
                            <span class="topRow">
                            <span class="cur"><i class="fa fa-rupee"></i></span>
                                <span class="price">3000</span>
                            </span>
                            <p class="duration">(3 Months)</p>
                        </li>
                        <li>
                            <span class="listings hvr-glow"><span class="fa icon-draw-check-mark"></span>Unlimited Access to All Exams </span>
                        </li>
                        <li>
                            <span class="fa icon-draw-check-mark"></span>Franchise for Sale Website Uniquely designed
                            for Franchise only
                        </li>
                        <li>
                            <span class="fa icon-draw-check-mark"></span>Franchise for Sale Website Uniquely designed
                            for Franchise only
                        </li>
                        <li>
                            <a href="JavaScript:Void(0);" class="listingBtn silver hvr-push" id="bronzeID">Checkout</a>
                        </li>
                        </ul>
                </div>
            </div>
    </div>
    <div class="row" style="margin-top: 35px;">
        <div class="offset-2 col-lg-4 col-md-6 col-sm-12 col-xs-12 offsettop35">
                    <div class="priceList hvr-grow text-center bronze_package">
                        <div class="priceHeader normal">
                            <p class="priceTittle"> <span class="subTitle">MOCK EXAM</span></p>
                            <span class="shape"></span>
                        </div>
                        <ul class="priceListing">
                            <li>
                                <span class="topRow">
                                    <span class="cur"><i class="fa fa-rupee"></i></span>
                                    <span class="price">100 <span class="each">each</span></span>
                                </span>
                                <p class="duration">(3 days)</p>
                            </li>
                            <li>
                                <span class="listings hvr-glow"><span class="fa icon-draw-check-mark"></span>Unlimited Access to All Exams </span>
                            </li>

                            <li>
                            <label class="containerx">SELECT  ALL
                                <input type="checkbox"  class="selectAllMockExam">
                                <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                            <label class="containerx">NEET Mock Exam 1
                                <input type="checkbox" class="selectMockExam">
                                <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                            <label class="containerx">NEET Mock Exam 2
                                <input type="checkbox"  class="selectMockExam">
                                <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                            <label class="containerx">NEET Mock Exam 3
                                <input type="checkbox" class="selectMockExam">
                                <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                            <label class="containerx">NEET Mock Exam 4
                                <input type="checkbox" class="selectMockExam">
                                <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                            <label class="containerx">NEET Mock Exam 5
                                <input type="checkbox"  class="selectMockExam">
                                <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                            <label class="containerx">NEET Mock Exam 6
                                <input type="checkbox"  class="selectMockExam">
                                <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                            <label class="containerx">NEET Mock Exam 7
                                <input type="checkbox"  class="selectMockExam">
                                <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                            <label class="containerx">NEET Mock Exam 8
                                <input type="checkbox"  class="selectMockExam">
                                <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                            <label class="containerx">NEET Mock Exam 9
                                <input type="checkbox"  class="selectMockExam">
                                <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                            <label class="containerx">NEET Mock Exam 10
                                <input type="checkbox"  class="selectMockExam">
                                <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                            <label class="containerx">NEET Mock Exam 11
                                <input type="checkbox"  class="selectMockExam">
                                <span class="checkmark"></span>
                                </label>
                            </li>


                        </ul>
                    </div>
        </div>

        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12 offsettop35">
                    <div class="priceList hvr-grow text-center bronze_package">
                        <div class="priceHeader normal">
                            <p class="priceTittle">  <span class="subTitle">Previous Year Exam</span></p>
                            <span class="shape"></span>
                        </div>
                        <ul class="priceListing">
                            <li>
                                <span class="topRow">
                                    <span class="cur"><i class="fa fa-rupee"></i></span>
                                    <span class="price">100 <span class="each">each</span></span>
                                </span>
                                <p class="duration">(3 days)</p>
                            </li>
                            <li>
                                <span class="listings hvr-glow"><span class="fa icon-draw-check-mark"></span>Unlimited Access to All Exams </span>
                            </li>

                            <li>
                            <label class="containerx">SELECT ALL
                                <input type="checkbox"  class="selectAllYearExam">
                                <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                            <label class="containerx">NEET Year Exam 2018 Set A
                                <input type="checkbox"  class="selectYearExam">
                                <span class="checkmark"></span>
                                </label>
                            </li>

                            <li>
                            <label class="containerx">NEET Year Exam 2018 Set B
                                <input type="checkbox"  class="selectYearExam">
                                <span class="checkmark"></span>
                                </label>
                            </li>

                            <li>
                            <label class="containerx">NEET Year Exam 2017 Set A
                                <input type="checkbox"  class="selectYearExam">
                                <span class="checkmark"></span>
                                </label>
                            </li>

                            <li>
                            <label class="containerx">NEET Year Exam 2017 Set B
                                <input type="checkbox"  class="selectYearExam">
                                <span class="checkmark"></span>
                                </label>
                            </li>

                            <li>
                            <label class="containerx">NEET Year Exam 2016 Set A
                                <input type="checkbox"  class="selectYearExam">
                                <span class="checkmark"></span>
                                </label>
                            </li>

                            <li>
                            <label class="containerx">NEET Year Exam 2016 Set B
                                <input type="checkbox"  class="selectYearExam">
                                <span class="checkmark"></span>
                                </label>
                            </li>

                            <li>
                            <label class="containerx">NEET Year Exam 2015 Set A
                                <input type="checkbox"  class="selectYearExam">
                                <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                            <label class="containerx">NEET Year Exam 2015 Set B
                                <input type="checkbox"  class="selectYearExam">
                                <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                            <label class="containerx">NEET Year Exam 2015 Set C
                                <input type="checkbox"  class="selectYearExam">
                                <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                            <label class="containerx">NEET Year Exam 2015 Set D
                                <input type="checkbox" class="selectYearExam">
                                <span class="checkmark"></span>
                                </label>
                            </li>
                            <li>
                            <label class="containerx">NEET Year Exam 2016 Set C
                                <input type="checkbox"  class="selectYearExam">
                                <span class="checkmark"></span>
                                </label>
                            </li>


                        </ul>
                    </div>


        </div>
        <div style="margin-top: 35px;width: 100%;text-align: center;" class="bronze_package">
        <div style="color:greenyellow ; font-weight:500" >
            Total  Rs. <span id="yearexamcost"> 0 </span>
        </div>

         <a href="JavaScript:Void(0);" class="listingBtn normal hvr-push" id="bronzeID">Checkout</a>
        </div>
    </div>
</div>


<script>
$(document).ready(function() {
    $('#yearexamcost').text('0');
  

    $('.selectAllMockExam').change(function() {
        if($(this).is(":checked")) {
            var totalCheck =  $('.selectMockExam').filter(':checked').length;
            var totalCheckVal = totalCheck * 100
            var cuurunt_cost =$('#yearexamcost').text()
            var cuurunt_cost_value = parseInt(cuurunt_cost)
            var finalcost =cuurunt_cost_value - totalCheckVal + 1100
            $('.selectMockExam').prop('checked',true)

           $('#yearexamcost').text(finalcost);
        }else{
            var totalCheck =  $('.selectMockExam').filter(':checked').length;
            var totalUnchecked = 11 - totalCheck
            var totalUncheckVal = totalUnchecked * 100
            var cuurunt_cost =$('#yearexamcost').text()
            var cuurunt_cost_value = parseInt(cuurunt_cost)
            var finalcost =cuurunt_cost_value + totalUncheckVal - 1100


            $('.selectMockExam').prop('checked',false)
           $('#yearexamcost').text(finalcost);
        }

    });


    $('.selectMockExam').change(function() {
        var val =$('#yearexamcost').text()

        var mockcost = parseInt(val)
        if($(this).is(":checked")) {

            mockcost = mockcost + 100;
            $('#yearexamcost').text(mockcost);

        }else{
            mockcost = mockcost - 100;
            $('#yearexamcost').text(mockcost);
        }

    });




     $('.selectAllYearExam').change(function() {
        if($(this).is(":checked")) {

            var totalCheck =  $('.selectYearExam').filter(':checked').length;
            var totalCheckVal = totalCheck * 100
            var cuurunt_cost =$('#yearexamcost').text()
            var cuurunt_cost_value = parseInt(cuurunt_cost)
            var finalcost =cuurunt_cost_value - totalCheckVal + 1100
            $('.selectYearExam').prop('checked',true)
            $('#yearexamcost').text(finalcost);

        }else{


            var totalCheck =  $('.selectYearExam').filter(':checked').length;
            var totalUnchecked = 11 - totalCheck
            var totalUncheckVal = totalUnchecked * 100
            var cuurunt_cost =$('#yearexamcost').text()
            var cuurunt_cost_value = parseInt(cuurunt_cost)
            var finalcost =cuurunt_cost_value + totalUncheckVal - 1100
            $('.selectYearExam').prop('checked',false)
            $('#yearexamcost').text(finalcost);

        }

    });


    $('.selectYearExam').change(function() {
        var val =$('#yearexamcost').text()

        var yearexamcost = parseInt(val)
        if($(this).is(":checked")) {

            yearexamcost = yearexamcost + 100;
            $('#yearexamcost').text(yearexamcost);

        }else{

            yearexamcost = yearexamcost - 100;
            $('#yearexamcost').text(yearexamcost);
        }

    });
});
</script>

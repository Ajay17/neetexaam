<div class="col-md-12">
        <h2 class="text-center primeText">Question Set</h2>
        <div class="my-dashboard">
            <div class="container-fluid">
                    <?php if(!empty($questionSets)){
                            foreach ($questionSets as $index => $questionSet) : ?>
                            <div class="row offset">
                                <?php foreach ($questionSet['question_sets'] as $index => $indexQuestionSet) : ?>
                                    <div class="col-md-4" style="display: table; height: 160px; overflow: hidden;">
                                        <div class="box" style="display: table-cell; vertical-align: middle;">
                                            <p class="text-heading" style="text-align: center;"><?= $indexQuestionSet['QuestionSet']['set_name'] ?></p>
                                            <?php $student_test_page = Router::url(['controller' => 'students' , 'action' => 'student_test_page/'.$indexQuestionSet['QuestionSet']['id']]) ?>
                                            <a class="more-info" href="<?= $student_test_page ?>">Start Test <span class="fa fa-arrow-right"></span></a>
                                        </div>
                                    </div>
                                <?php endforeach;?>
                            </div>    
                            <?php endforeach;?>
                    <?php } else { ?> 
                        <h2 class="text-center primeText">No Question Set Found</h2>
                    <?php } ?>    
            </div> 
        </div>
</div>                       
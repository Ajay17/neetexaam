<div class="container" style="margin-top:30px;margin-bottom:30px">

<div class="row">
<div class="col-md-4 leftBox">
	 <div style="margin-top:70px">
      <img id="profile_pic" width="250px" src='../app/webroot/<?php echo  $student_profile['Student']['profile_url'] ?>'  alt="No Profile Pic" />
    </div>
    <div style="margin-top: 30px">

 <input type="file"onchange="upload_image(this, '<?php echo $student_profile['Student']['id'] ?>')"   accept="image/*">
</div>
<h5 id="img_msg" style="display:none"> file upload !!!<h5>
</div>
<div class="col-md-8 rightBox" id="studentProfile">
<h2 class="text-center primeText">Profile</h2>
<div class="container myForm signUpForm d-flex flex-column">

<div class="row">
<div class="col">
<div class="form-group">
<input type="text" id="studentProfileName" name="studentProfileName" value='<?php echo $student_profile['Student']['contact_name'] ?>' onfocusout="myFocusOut('studentProfileNameError' ,'studentProfileName')" class="form-control" required>
<label class="form-control-placeholder" for="name">Name</label>
<span class="error" id="studentProfileNameError"><span class="fa fa-info-circle"></span> Error Message</span> 
</div>
</div>
</div> 



<div class="row">
<div class="col">
<div class="form-group">
<input type="text" id="studentProfileEmailId" name="studentProfileEmailId"  value='<?php echo $student_profile['Student']['email'] ?>' onfocusout="myFocusOut('studentProfileEmailidError' ,'studentProfileEmailId')" class="form-control" required>
<label class="form-control-placeholder" for="name">Email</label>
<span class="error" id="studentProfileEmailidError"><span class="fa fa-info-circle"></span> Error Message</span> 
</div>
</div>
</div> 


<div class="row">
<div class="col">
<div class="form-group">
<input type="text" id="studentProfilePhone" name="studentProfilePhone"  value='<?php echo $student_profile['Student']['mobile'] ?>'  onfocusout="myFocusOut('studentProfilePhoneError' ,'studentProfilePhone')" class="form-control" required>
<label class="form-control-placeholder" for="name">Phone number</label>
<span class="error" id="studentProfilePhoneError"><span class="fa fa-info-circle"></span> Error Message</span> 
</div>
</div>
</div> 


<div class="row">
<div class="col">
<div class="form-group">
<input type="text" id="studentProfileCity" name="studentProfileCity" value='<?php echo $student_profile['Student']['city'] ?>' onfocusout="myFocusOut('studentProfileCityError' ,'studentProfileCity')" class="form-control" required>
<label class="form-control-placeholder" for="name">City</label>
<span class="error" id="studentProfileCityError"><span class="fa fa-info-circle"></span> Error Message</span> 
</div>
</div>
</div> 




<div class="alert alert-danger "  style="display: none;" id="loginerror">Invalid EmailId or Password</div> 
<div class="modal-footer justify-content-center">
<span class="loader"></span>
<span class="success_msg">
<strong>Profile Update !!!</strong> 
</span>

<?php $institute_url =Router::url(["controller" => "institutes", "action" => "institute_dashboard"]);?>
<?php $student_url =Router::url(["controller" => "students", "action" => "student_dashboard"]);?>
<?php $url =Router::url(["controller" => "homes", "action" => "login"]);?>


<button class=" btn_click hvr-ripple-out primeBg caps btn" onclick="editProfile()">Update</button>



</div>
</div>
</div>
</div>
</div>

<script>



function upload_image(input , id ){
    var file_data =input.files[0];
    var form_data = new FormData();   
    form_data.append('id' ,id);
    form_data.append('files', file_data );
    var url=  '<?= Router::url(["controller" => "Students" , "action"=>"updateStudentProfilePic"]);?>';
    $.ajax({
            url:url,
            type:'POST',
            contentType: false,
            data :form_data,
            processData: false,
            success:function(data){
             preview(data );
             $('#img_msg').text('file uploaded !!!');
             $('#img_msg').show();
            },error: function (errorthrown) {
                 
            }
          });
      
  }
  function preview(url ){
    
    console.log("preview url is "+url)
    $('#profile_pic').attr('src', url);
  }


function editProfile(){
  $('.loader').show()
      var option={};
        option['id']= '<?php echo $student_profile['Student']['id'] ?>';
      $.each($("#studentProfile").find(':input'),function(index, data){
        option[$(data).attr("name")]=$(data).val();
      });
     
      console.log(option)
      var url= '<?= Router::url(["controller" => "Students" , "action"=>"updateStudentProfile"]);?>';
      $.ajax({
        url:url,
        type:'POST',
        data :{'data':option},
        success:function(data){
          $('.loader').hide()
          if(data == 1){
                      $(".success_msg").show();
                     
                      }else{
                      }
        },error: function (errorthrown) {
          $('.loader').hide()
          alert("Failed To Update !!!");
        }
      })
    }




	</script>

<div class=" container-fluid" >
 <?php $url = Router::url(['controller' => 'AdminQuestions', 'action' => 'createQuestion'])?>
 <form class="col-md-12"  action="<?php echo $url ?>" method="post" enctype="multipart/form-data">
   <h3 > <?php echo $message ?></h3>
   <h3 style="margin-bottom: 30px" > Create  Question</h3>
   <div class="form-group">
    <label for="exampleFormControlSelect1">Select Question Set</label>
    <select style="width:48%!important" class="form-control" id="exampleFormControlSelect1" name="question_set_id">
      <?php if(!empty($all_question_sets)){

       foreach ($all_question_sets as $index => $question_set) : ?>
         <option value='<?php echo $question_set['QuestionSet']['id'] ?>'><?php echo $question_set['QuestionSet']['set_name'] ?></option>
       <?php endforeach; } ?>
       

     </select>
   </div>
   
 

  <div class="row" >
    <div class="col-6">
        <div class="form-group">
          <label>Question</label>
          <textarea class="form-control"  name="question" ></textarea>
        </div>

    </div>
    <div class="col-6" style="margin-top: 25px">
     <div >
     
      <input type="file" onchange="readURL(this,'ques')" name="0"  accept="image/*">
    </div>
    <div>
      <img id="ques" />
    </div>
  </div>
</div>

  <div class="row" >
    <div class="col-6">
      <div class="form-group">
        <label>Option1</label>
        <input style="width:95%!important" type="text" class="option1"  name="option1" > 
        <input class="radio-button option" type="radio" data-value="option1" name="option" value="option 1 is answer"/>
     

      </div>
    </div>
    <div class="col-6" style="margin-top: 25px">
     <div >
       <input type="file" onchange="readURL(this,'opt1')" name="1" id="option1"  accept="image/*">
     </div>
      <div>
       <img id="opt1"  />
      </div>
    </div>
  </div>



  <div class="row" >
    <div class="col-6">
      <div class="form-group">
      <label>Option2</label>
      <input style="width:95%!important"  type="text" class="option2"  name="option2">
      <input class="radio-button option" type="radio" data-value="option2" name="option" value="option 2 is answer"/>
    </div>
    </div>
    <div class="col-6" style="margin-top: 25px">
     <div >
    
      <input type="file" onchange="readURL(this,'opt2')" name="2" id="option2" accept="image/*">
    </div>
    <div>
      <img id="opt2" />
    </div>
  </div>
</div>


 <div class="row" >
    <div class="col-6">
      <div class="form-group">
    <label>Option3</label>
    <input style="width:95%!important"  type="text" class="option3" name="option3">
    <input class="radio-button option" type="radio" data-value="option3"name="option" value="option 3 is answer"/>
  </div>
    </div>
    <div class="col-6" style="margin-top: 25px">
     <div >
    
      <input type="file" onchange="readURL(this,'opt3')" name="3" id="option3" accept="image/*">
    </div>
    <div>
      <img id="opt3" />
    </div>
  </div>
</div>


 <div class="row" >
    <div class="col-6">
      <div class="form-group">
    <label>Option4</label>
    <input style="width:95%!important"  type="text" class="option4" name="option4">
    <input class="radio-button option" type="radio" data-value="option4" name="option" value="option 4 is answer"/>
  </div>
    </div>
    <div class="col-6" style="margin-top: 25px">
     <div >
    
      <input type="file" onchange="readURL(this,'opt4')" name="4" id="option4"  accept="image/*">
    </div>
    <div>
      <img id="opt4" />
    </div>
  </div>
</div>




<div class="row">
  <div class="col-6">
    <div class="form-group">
      <label for="exampleFormControlInput1">Correct Answer</label>
      <input type="text" class="form-control correct-answer"  id="exampleFormControlInput1"  name="correct_answer">
    </div>
  </div>
  <div class="col-6">
   <div class="form-group">
    <label for="exampleFormControlInput1">Subject</label>
    <input type="text" class="form-control"  id="exampleFormControlInput1" name="subject" >
  </div>
</div>
</div>  

<div class="row">
  <div class="col-6">
   <div class="form-group">
    <label for="exampleFormControlInput1">Topic</label>
    <input type="text" class="form-control"  id="exampleFormControlInput1" name="topic">
  </div>
</div>
<div class="col-6">
 <div class="form-group">
  <label for="exampleFormControlInput1">Active</label>
  <!--<input type="text" class="form-control"  id="exampleFormControlInput1" name="is_active"> -->
  <input class="radio-button" type="radio" name="is_active" value="true"/>
  <label for="exampleFormControlInput1">DeActive</label>
  <!--<input type="text" class="form-control"  id="exampleFormControlInput1" name="is_active"> -->
  <input class="radio-button" type="radio" name="is_active" value="false"/>
 
</div>
</div>
</div> 

<div class="row">
  <div class="col-6">
    <div class="form-group">
     <label for="exampleFormControlInput1">Explanation</label>
    <!--<input type="text" class="form-control"  id="exampleFormControlInput1" name="explanation"> -->
    <textarea  class="form-control" id="exampleFormControlInput1" name="explanation" ></textarea>
   </div>
  </div>
  <div class="col-6">
   <div class="form-group">
    <div class="col-6" style="margin-top: 25px">
     <div >
       <input type="file" onchange="readURL(this,'explainationimage')" name="5" id="explanation"  accept="image/*">
     </div>
      <div>
       <img id="explainationimage"  />
      </div>
    </div>
  </div>
</div>

</div> 







<button type="submit" class="btn btn-primary mb-2">Submit</button>
</form>
</div>



<div class="container" style="margin-top: 30px" >
  <h3>Update Question</h3>
  <h5 id="img_msg" style="display:none"> file upload !!!<h5>
  <img id="upl_pre" />
  <div class="table table-responsive">
    <table class="table table-bordered">
      <thead class="table-primary">
        <tr>

         <th>Action</th>

         <th>Sno.</th>
         <th>Id</th>
         <th> Question Set</th>
         <th>Question</th>
         <th> Question Image </th>
         <th>Option1</th>
         <th>Option1 Image</th>
         <th> Option2</th>
         <th>Option2 Image</th>
         <th>Option3</th>
         <th>Option3 Image</th>
         <th>Option4</th>
         <th>Option4 Image</th>
         <th> Correct Answer</th>
         <th>Explanation</th>
         <th>Subject</th>
         <th> Topic</th>
         <th>IsActive</th>

       </tr>
     </thead>
     <tbody class="scrollx">
       <?php if(!empty($all_questions)){
         foreach ($all_questions as $index => $question) : ?>
          <tr id="<?php echo $question['Question']['id']?>">
            <td class="d-flex">
              <button class="offsetmini fa fa-edit btn btn-info" onclick='editQuestion("<?php echo $question['Question']['id'] ?>")'></button>
              <button class="offsetmini fa fa-trash btn btn-danger" onclick='deleteQuestion("<?php echo $question['Question']['id'] ?>")'></button>
            </td>
            <td><?php echo  $index +1  ?></td>

            <td style="width:30px!important"><input style="width:30px!important" class="value" name="question_id" value="<?php echo $question['Question']['id'] ?>" readonly/></td>
            <td>
              <select   name="question_set_id" class="value" >
                <option value='<?php echo $question['QuestionSet']['id'] ?>'><?php echo $question['QuestionSet']['set_name'] ?></option>
                <?php if(!empty($all_question_sets)){
                 foreach ($all_question_sets as $index => $question_set) :
                  if($question_set['QuestionSet']['id'] !=  $question['QuestionSet']['id']){
                    ?>
                    <option value='<?php echo $question_set['QuestionSet']['id'] ?>'><?php echo $question_set['QuestionSet']['set_name'] ?></option>
                  <?php } endforeach; } ?>
                </select>
              </td>
              <td><input  name="question" class="value" value="<?php echo $question['Question']['question'] ?>"/></td>
              <td>

              <button onclick='preview(" <?php echo  $this->webroot.$question['Question']['question_image'] ?>")'>View</button>
          
              <input type="file" onchange="upload_image(this, '<?php echo $question['Question']['id'] ?>' ,'question_image','0')"   accept="image/*">
             
              <button  onclick="remove_image('<?php echo $question['Question']['id'] ?>' ,'question_image')">Remove </button>
              </td>
              <td><input  name="option1" class="value" value="<?php echo $question['Question']['option1'] ?>"/></td>
              
              <td>

              <button onclick='preview(" <?php echo $this->webroot.$question['Question']['option1_image'] ?>")'>View</button>
          
              <input type="file" onchange="upload_image(this, '<?php echo $question['Question']['id'] ?>' ,'option1_image','1')"   accept="image/*">
             
              <button  onclick="remove_image('<?php echo $question['Question']['id'] ?>' ,'option1_image')">Remove </button>
              </td>



              <td><input  name="option2" class="value" value="<?php echo $question['Question']['option2'] ?>"/></td>
              
                
              <td>

              <button onclick='preview(" <?php echo  $this->webroot.$question['Question']['option2_image'] ?>")'>View</button>
          
              <input type="file" onchange="upload_image(this, '<?php echo $question['Question']['id'] ?>' ,'option2_image' ,'2')"   accept="image/*">
             
              <button  onclick="remove_image('<?php echo $question['Question']['id'] ?>' ,'option2_image')">Remove </button>
              </td>

              
              
              
              
              <td><input name="option3" class="value" value="<?php echo $question['Question']['option3'] ?>"/></td>
              
              <td>

              <button onclick='preview(" <?php echo  $this->webroot.$question['Question']['option3_image'] ?>")'>View</button>
          
              <input type="file" onchange="upload_image(this, '<?php echo $question['Question']['id'] ?>' ,'option3_image' ,'3')"   accept="image/*">
             
              <button  onclick="remove_image('<?php echo $question['Question']['id'] ?>' ,'option3_image')">Remove </button>
              </td>
              
              
              
              <td><input  name="option4" class="value" value="<?php echo $question['Question']['option4'] ?>"/></td>
              
              <td>

              <button onclick='preview(" <?php echo  $this->webroot.$question['Question']['option4_image'] ?>")'>View</button>
          
              <input type="file" onchange="upload_image(this, '<?php echo $question['Question']['id'] ?>' ,'option4_image' ,'4')"   accept="image/*">
             
              <button  onclick="remove_image('<?php echo $question['Question']['id'] ?>' ,'option4_image')">Remove </button>
              </td>
              
              
              
              <td><input name="correct_answer" class="value" value="<?php echo $question['Question']['correct_answer'] ?>"/></td>
              
              <td>
              
              <input  name="explanation" class="value" value="<?php echo $question['Question']['explanation'] ?>"/>
              <button onclick='preview(" <?php echo  $this->webroot.$question['Question']['explanation'] ?>")'>View</button>
          
              <input type="file" onchange="upload_image(this, '<?php echo $question['Question']['id'] ?>' ,'explanation' ,'5')"   accept="image/*">
             
              <button  onclick="remove_image('<?php echo $question['Question']['id'] ?>' ,'explanation')">Remove </button>
              
              
              </td>
              
              
              <td><input  name="subject" class="value" value="<?php echo $question['Question']['subject'] ?>"/></td>
              <td><input  name="topic" class="value" value="<?php echo $question['Question']['topic'] ?>"/></td>
              <td><input  name="is_active" class="value" value="<?php echo $question['Question']['is_active'] ?>"/></td>

            </tr>

          <?php endforeach; } ?>


        </tbody>
      </table>
    </div>
  </div>

<script>
  $(document).ready(function () {
    $('.option').click(function () {
        if ($(this).is(':checked')) {
           // alert($(this).attr("name"));
          // alert($(this).data().value);
            if($("."+$(this).data().value).val()!="" || $("#"+$(this).data().value).val()!=""){
              $(".correct-answer").val($(this).data().value);
            }else{
              alert("This Option is empty,please fill this option then set it correct.")
              $(this).prop('checked', false);
            }
        }
    });
});
</script>

<script>
    function editQuestion(id){
      var option={};
      $.each($("#"+id).find('.value'),function(index, data){
        option[$(data).attr("name")]=$(data).val();
      });
      var url= '<?= Router::url(["controller" => "AdminQuestions" , "action"=>"updateQuestion"]);?>';
      $.ajax({
        url:url,
        type:'POST',
        data :{'data':option},
        success:function(data){
          alert('succes')
        },error: function (errorthrown) {
          alert("Failed To Update !!!");
        }
      })
    }

    function deleteQuestion(id){
     var url=  '<?= Router::url(["controller" => "AdminQuestions" , "action"=>"deleteQuestion"]);?>';
     $.ajax({
      url:url,
      type:'POST',
      data :{'data':id},
      success:function(data){
        alert('succes')
        $("#"+id).hide();
      },
      error: function (errorthrown) {

        alert("Failed To Update !!!");
      }
    })
   }





   function readURL(input ,id) {
     console.log("input is "+input);
     console.log("id is "+id);
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        $('#'+id)
        .attr('src', e.target.result);
      };
      console.log(input.files[0]);
      reader.readAsDataURL(input.files[0]);
    }
  }

  function preview(url ){
    console.log("preview url is "+url)
    $('#upl_pre').attr('src', url);
  }


  function upload_image(input , id , field ,name){
    var file_data =input.files[0];
    var form_data = new FormData();   
    form_data.append('id' ,id);
    form_data.append('field' , field);
    form_data.append('name' , name);
    form_data.append('files', file_data );
    var url=  '<?= Router::url(["controller" => "AdminQuestions" , "action"=>"updateImage"]);?>';
    $.ajax({
            url:url,
            type:'POST',
            contentType: false,
            data :form_data,
            processData: false,
            success:function(data){
             preview(data );
             $('#img_msg').text('file uploaded !!!');
             $('#img_msg').show();
            },error: function (errorthrown) {
                 
            }
          });
      
  }


  function remove_image( id , field){
      var option={};
      option['id'] =id;
      option['field'] = field;
      var url=  '<?= Router::url(["controller" => "AdminQuestions" , "action"=>"removeImage"]);?>';
      $.ajax({
              url:url,
              type:'POST',
              data :{'data' :option},
              success:function(data){
                    preview('' );
                    $('#img_msg').text('file removed !!!');
                    $('#img_msg').show();
                  }, error: function (errorthrown) {
                 
                  }
              });
    }


</script>
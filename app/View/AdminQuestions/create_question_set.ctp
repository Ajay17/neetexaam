 <div class=" container-fluid">
     <div class="col-md-1">
     </div>
     <h1 style="margin-top:50px ; margin-bottom:50px"> <?php echo $message ?></h1>
     <?php $url = Router::url(['controller' => 'AdminQuestions', 'action' => 'createQuestionSet'])?>
<form class="col-md-10"  action="<?php echo $url ?>" method="post" >
<h3> Create  Question Set</h3>
  
  
    <div class="form-group">
    <label for="exampleFormControlInput1">Question Set Name</label>
    <input type="text" class="form-control" required id="exampleFormControlInput1" name="set_name"/>
  </div>


   <button type="submit" class="btn btn-primary mb-2">Submit</button>
</form>
   
</div>

<div class="container" style="margin-top: 30px" >
  <h3>Update Question Set</h3>


 <div class="table table-responsive">
    <table class="table table-bordered">
      <thead class="table-primary">
        <tr>

         <th>Action</th>

         <th>Sno.</th>
        
         <th>Id</th>
         <th>Question Set Name</th>
     </tr>
 </thead>

  <tbody class="scrollx">
       <?php if(!empty($all_question_sets)){
     
         foreach ($all_question_sets as $index => $question_set) : ?>
                <tr id="<?php echo $question_set['QuestionSet']['id']?>">
                    <td class="d-flex">
              <button class="offsetmini fa fa-edit btn btn-info" onclick='editQuestionSet("<?php echo $question_set['QuestionSet']['id'] ?>")'></button>
              <button class="offsetmini fa fa-trash btn btn-danger" onclick='deleteQuestionSet("<?php echo $question_set['QuestionSet']['id'] ?>")'></button>
            </td>
            <td><?php echo  $index +1  ?></td>

            <td><input  class="value" name="question_set_id" value="<?php echo $question_set['QuestionSet']['id'] ?>" readonly/></td>
             <td><input  class="value" name="set_name" value="<?php echo $question_set['QuestionSet']['set_name'] ?>" /></td>
          
                </tr>
              <?php endforeach; } ?>


        </tbody>
         

</table>
</div>
</div>


  <script type="text/javascript">
      
function editQuestionSet(id){
      var option={};
      $.each($("#"+id).find('.value'),function(index, data){
        option[$(data).attr("name")]=$(data).val();
      });
      var url=  '<?= Router::url(["controller" => "AdminQuestions" , "action"=>"updateQuestionSet"]);?>';
      console.log(option);
      $.ajax({
        url:url,
        type:'POST',
        data :{'data':option},
        success:function(data){
          alert('succes')
        },
        error: function (errorthrown) {

          alert("Failed To Update !!!");
        }
      })
    }

    function deleteQuestionSet(id){
     var url=  '<?= Router::url(["controller" => "AdminQuestions" , "action"=>"deleteQuestionSet"]);?>';
     $.ajax({
      url:url,
      type:'POST',
      data :{'data':id},
      success:function(data){
        alert('succes')
        $("#"+id).hide();
      },
      error: function (errorthrown) {

        alert("Failed To Update !!!");
      }
    })
   }

  </script>
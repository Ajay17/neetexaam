<div class=" container-fluid">
     <div class="col-md-1">
     </div>
     <h1 style="margin-top:50px ; margin-bottom:50px"> <?php echo $message ?></h1>
     <?php $url = Router::url(['controller' => 'AdminFaq', 'action' => 'pagePricing'])?>
<form class="col-md-10"  action="<?php echo $url ?>" method="post" >
     
     <h3>Add Pricing Page</h3>
    <div class="form-group">
     <label for="exampleFormControlInput1">Pricing Page Information</label>
     <input type="text" class="form-control" required id="exampleFormControlInput1" name="exam_name" placeholder="Exam Name"/>
     <input type="text" class="form-control" required id="exampleFormControlInput2" name="exam_type" placeholder="Exam Type"/>

     <input type="text" class="form-control" required id="exampleFormControlInput3" name="price" placeholder="Price"/>
     <input type="text" class="form-control" required id="exampleFormControlInput4" name="validity" placeholder="Validity"/>
   </div>
   <button type="submit" class="btn btn-primary mb-2">Submit</button>
</form>
   
</div>

<div class="container" style="margin-top: 30px" >
  <h3>Update Page Pricing</h3>

 
  <div class="table table-responsive">
    <table class="table table-bordered">
      <thead class="table-primary">
        <tr>
         <th>Action</th>
         <th>Sno.</th>
         <th>Id</th>
         <th> Exam Name </th>
         <th> Exam Type </th>
         <th> Price </th>
         <th> Validity </th>
     </tr>
 </thead>

  <tbody class="scrollx">
       <?php 
    
       if(!empty($all_page_price)){
     
         foreach ($all_page_price as $index => $page_price) : ?>
           <tr id="<?php echo $page_price['PagePrice']['id']?>">
             <td class="d-flex">
              <button class="offsetmini fa fa-edit btn btn-info" onclick='editPagePrice("<?php echo $page_price['PagePrice']['id'] ?>")'></button>
              <button class="offsetmini fa fa-trash btn btn-danger" onclick='deletePagePrice("<?php echo $page_price['PagePrice']['id'] ?>")'></button>
            </td>
            <td><?php echo  $index +1  ?></td>

             <td><input  class="value" name="page_price_id" value="<?php echo $page_price['PagePrice']['id'] ?>" readonly/></td>
             <td><input  class="value" name="exam_name" value="<?php echo $page_price['PagePrice']['exam_name'] ?>" /></td>
             <td><input  class="value" name="exam_type" value="<?php echo $page_price['PagePrice']['exam_type'] ?>" /></td>

             <td><input  class="value" name="price" value="<?php echo $page_price['PagePrice']['price'] ?>" /></td>
             <td><input  class="value" name="validity" value="<?php echo $page_price['PagePrice']['validity'] ?>" /></td
            </tr>
              <?php endforeach; } ?>


        </tbody>
         

</table>
</div>
</div>

<script type="text/javascript">
      
  function editPagePrice(id){
      var option={};
      $.each($("#"+id).find('.value'),function(index, data){
        option[$(data).attr("name")]=$(data).val();
      });
      var url=  '<?= Router::url(["controller" => "AdminFaq" , "action"=>"updatePagePrice"]);?>';
      console.log(option);
      $.ajax({
        url:url,
        type:'POST',
        data :{'data':option},
        success:function(data){
          console.log(data);
          alert('succes')
        },
        error: function (errorthrown) {
          alert("Failed To Update !!!");
        }
      })
    }

    function deletePagePrice(id){
     var url=  '<?= Router::url(["controller" => "AdminFaq" , "action"=>"deletePagePrice"]);?>';
     $.ajax({
      url:url,
      type:'POST',
      data :{'data':id},
      success:function(data){
        alert('succes')
        $("#"+id).hide();
      },
      error: function (errorthrown) {
        alert("Failed To Update !!!");
      }
    })
   }

  </script>
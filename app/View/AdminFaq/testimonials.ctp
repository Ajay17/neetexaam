<div class="container" style="margin-top: 30px" >
  <h3>Update Testimonials</h3>
    <div class="table table-responsive">
    <?php $paginator = $this->Paginator; ?>
        <table class="table table-bordered">
            <thead class="table-primary">
                    <tr>
                        <th>Action</th>
                        <th> Sno. </th>
                        <th> Person Name </th>
                        <th> Comment </th>
                        <th> Rating</th>
                        <th> User Type </th>
                    </tr>
            </thead>

            <tbody class="scrollx">
            <?php 
                if(!empty($testimonials)){
                    foreach ($testimonials as $index => $testimonial) : ?>
                        <tr id="<?php echo $testimonial['Testimonial']['id']?>">
                                <td class="d-flex">
                                <button class="offsetmini fa fa-trash btn btn-danger" onclick='deleteTestimonial("<?php echo $testimonial['Testimonial']['id'] ?>")'></button>
                                <?php if($testimonial['Testimonial']['status'] == 1) { ?>
                                    <button id="updateTestimonialBtn<?= $testimonial['Testimonial']['id']; ?>" class="offsetmini fa fa-check-square  btn btn-success" onclick='updateTestimonial("<?php echo $testimonial['Testimonial']['id'] ?>")'></button>
                                <?php } ?>
                                </td>
                                <td><?php echo  $index +1  ?></td>
                                <td><input  class="value" name="name" value="<?php echo $testimonial['Testimonial']['name'] ?>" readonly/></td>
                                <td><input  class="value" name="comment " value="<?php echo $testimonial['Testimonial']['comment'] ?>" readonly/></td>
                                <td><input  class="value" name="rating" value="<?php echo $testimonial['Testimonial']['rating'] ?>" readonly/></td>
                                <td><input  class="value" name="user_type" value="<?php if($testimonial['Testimonial']['user_type'] == 1) { echo "Student"; } else {echo "Institute";} ?>" /></td>
                        </tr>
                    <?php endforeach;?>
                <?php } ?>    
            </tbody>
        </table>
                 <ul class="pagination">
                    <li class="page-item"><?php echo $paginator->first("First"); ?></li>
                    <?php if($paginator->hasPrev()){ ?>
                       <li class="page-item"><?php echo $paginator->prev("Prev"); ?></li>
                   <?php } ?>
                    <li class="page-item"><?php echo $paginator->numbers(array('modulus' => 2));  ?></li>
                    <?php if($paginator->hasNext()){ ?>
                       <li class="page-item"><?php echo $paginator->next("Next"); ?></li>
                    <?php } ?>
                </ul> 
            
    </div>
</div>

<script type="text/javascript">
      
  function deleteTestimonial(id){
    swal({
            title: "Are you sure?",
            text: "Do You want to Delete this Testimonial ??",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
    }).then((result) => {
        if (result.value) {
            var option={};
            $.each($("#"+id).find('.value'),function(index, data){
                option[$(data).attr("name")]=$(data).val();
            });
            var url=  '<?= Router::url(["controller" => "Testimonials" , "action"=>"delete_testimonial"]);?>';
            NProgress.start();
               $.ajax({
                    url:url,
                    type:'POST',
                    dataType: "JSON",
                    data :{'id':id},
                    success:function(result){
                        if(result.status){
                            swal("Deleted!", "Testimonial Data is Deleted.", "success");
                            $("#"+id).hide();
                            location.reload(true);
                        }else {
                            swal("Something Went Wrong!", "Enable to Delete Testimonial. Please Try Again.", "error");
                        }
                        NProgress.done();
                    },
                    error: function (e) {
                        swal("Something Went Wrong!", "Enable to Delete Testimonial. Please Try Again.", "error");
                        NProgress.done();
                    }
            })
        }                      
    });
    }

    function updateTestimonial(id){
        swal({
            title: "Are you sure?",
            text: "Do You want to Show This Testimonial in Home Page??",
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes, show it!",
            cancelButtonText: "No, cancel!",
            closeOnConfirm: false,
            closeOnCancel: false
    }).then((result) => {
        if (result.value) {
            var option={};
            $.each($("#"+id).find('.value'),function(index, data){
                option[$(data).attr("name")]=$(data).val();
            });
            var url=  '<?= Router::url(["controller" => "Testimonials" , "action"=>"change_testimonial_status"]);?>';
            NProgress.start();
               $.ajax({
                    url:url,
                    type:'POST',
                    dataType: "JSON",
                    data :{'id':id},
                    success:function(result){
                        if(result.status){
                            swal("Success!", "you can see this Testimonial is Home Page.", "success");
                            $("#updateTestimonialBtn"+id).hide();
                        }else {
                            swal("Something Went Wrong!", "Enable to Change Testimonial Status. Please Try Again.", "error");
                        }
                        NProgress.done();
                    },
                    error: function (errorthrown) {
                        swal("Something Went Wrong!", "Enable to Change Testimonial Status. Please Try Again.", "error");
                        NProgress.done();
                    }
            })
        }                      
    });
   }

  </script>
<div class="container">
  <h2>Construct Your Topic</h2>
  <form  action="<?php echo Router::url(['controller' => 'adminFaq','action' => 'saveTopic']); ?>" method="post" id="target">
    <div class="form-group">
      <label for="title">Title:</label>
      <input type="text" class="form-control" id="topic_title" placeholder="Enter Title" name="topic_title">
    </div>

    <div class="form-group">
      <label for="topic_subtitle">Subtitle</label>
      <input type="text" class="form-control" id="topic_subtitle" placeholder="Enter subtitle" name="topic_subtitle">
    </div>

    <div class="form-group">
      <label for="topic_description">Description</label>
      <input type="textarea" class="form-control" id="topic_description" placeholder="Enter Description" name="topic_description">
    </div>

    <div class="form-group">
      <label for="topic_youtube_link">YouTube Link</label>
      <input type="text" class="form-control" id="topic_youtube_link" placeholder="Enter You Tube Link" name="topic_youtube_link">
    </div>

    <div class="form-group">
      <label for="topic_main_content">Main Content</label>
      <input type="textarea" class="form-control" id="topic_main_content" placeholder="Enter Main Content" name="topic_main_content">
    </div>

    <div class="checkbox">
      <label><input type="checkbox" name="remember"> Remember me</label>
    </div>
    <button type="submit" class="btn btn-default">Submit</button>
  </form>
</div>

<script src="https://cdn.ckeditor.com/4.6.2/full/ckeditor.js"></script>
  <script>
   editor  =  CKEDITOR.replace('topic_main_content');
   editor1 =  CKEDITOR.replace('topic_description');
   

    $('#target').submit(function(event){
      $('#topic_description').val(editor.getData());
      $('#topic_main_content').val(editor1.getData());
      return true;
    })
  </script>
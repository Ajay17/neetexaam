<?php echo $this->element('header') ?>

<section  id="homeSection">
  <div class="banner">
    <div class="container">
      
      <div class="row">
        <div class="col-lg-7 col-md-6 col-sm-12 col-xs-12">
          <div class="bannerDisc">  

          </div>
        </div>
        <div class="col-lg-5 col-md-6 col-sm-12 col-xs-12">
        </div>
      </div>
    </div>
  </div>
  <div class="timer"></div>
</section>

<section class="animated wow fadeInUp">
  <div class="container">
    <div class="cardSection">
      <div class="card-decks">
        <div class="row">
          <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 offset25">
            <div class="cards ">
              <div class="cardTop">
                <span class="cardTittle">Free Mock Tests <span>free</span></span>
                <p class="cardDisc">  <button type="button" class="btn btn-success">Sign Up Now</button></p>
              </div>
              <div class="hvr-underline-from-center cardData">
								<ul class="listingCard">
									We are providing 4 NEET mock tests and 2 NEET Previous year questions absolutely FREE... Please register it and practice for your NEET exam.
								</ul>
              </div>
            </div>
					</div>
					<div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 offset25">
            <div class="cards ">
              <div class="cardTop">
								<span class="cardTittle">NEET Mock Test Series </span>
								<p class="cardDisc"><button type="button" class="btn btn-success">Buy</button></p>
            	</div>
							<div class="hvr-underline-from-center cardData">
								<ul class="listingCard">
									We are providing 50+ Mock Test series.  Whenever you are ready to test your konowledge just pay Rs.100 and test your readiness with detailed reports
									<div id="card2_more_data" style="display:none;">
										about your preparation towards NEET exams.
										<br>Please buy our Jumbo pack to get 1 Year access to our NEET Mock test series along with last 12 years question papers.  Here you can practice the exam as well as study mode to read and understand all the questions with answers.
										<li><a class=" hvr-forward pull-right"  id="card2_more_data_less_link">View less <i class="fa fa-long-arrow-right"></i></a></li>
									</div>
									<li><a class=" hvr-forward pull-right"  id="card2_more_data_more_link">View More <i class="fa fa-long-arrow-right"></i></a></li>
                </ul>
              </div>
            </div>
					</div>
          <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12 offset25">
            <div class="cards ">
              <div class="cardTop">
                <span class="cardTittle">All India NEET Mock Test</span>
                <p class="cardDisc"><button type="button" class="btn btn-success">Sign Up Now</button></p>
              </div>
              <div class="hvr-underline-from-center cardData">
                <ul class="listingCard">
                  Hey!!! This is absolutely free for all.  Please register yourself and participate in the All India NEET Mock test is being conducted by us and get your score/rank.
                  <div id="card3_more_data" style="display:none;">
										This will be a scheduled Test happens on Sundays as published in the website Notice board.  This will be conducted  continously as per the schedule.
										<li><a class=" hvr-forward pull-right"  id="card3_more_data_less_link">View less <i class="fa fa-long-arrow-right"></i></a></li>
                  </div>
                  <li><a class=" hvr-forward pull-right"  id="card3_more_data_more_link">View More <i class="fa fa-long-arrow-right"></i></a></li>
                </ul>
              </div>
						</div>
          </div> 
        </div>
     </div>
    </div>
  </div>
</section>
<!-- About Section -->
<section class="animated wow fadeInUp" id="aboutSection">
  <div class="container">
    <div class="aboutSection">
      <div class="row">
        <div class="col-md-12 text-center">
          <h2 class="mainHeading">About us</h2>
            <div class="hrTag">
              <img class="hrTag_pencil" src="img/hr.png" alt="hr">
            </div>
        </div>
      </div>
       <div class="row offset25">
        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
          <div class="imgArea">
            <img src="img/first.png" alt="first">
            <!-- <div class="hvr-outline-out overlay">
              <strong class="overlayBold">10</strong>
              <p class="overlayText">years of experiences</p>
              <p class="overlayTextNew">Helping customers keep
                comfortable at home through
            corporate infrastructures.</p>
            </div> -->
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
          <div class="contentArea">
            <!-- <p class="firstPara">Lorem ipsum dollor
            sit amet.</p> -->
            <p class="firstParaNew">We are here to help the students who are preparing for their NEET exam.
              We are providing a platform for the students to prepare and test their readiness to go for NEET Exam.
               We are also conducting scheduled Exam for the students to under go mock exam and the ranks will be published to know where they stands.
               Also we are providing previous years NEET exam questions and a list of Mock Exams to practice and get to know the analysis of your exams.
        </p>
         <!-- <a class="hvr-ripple-out primeBg caps btn radiusBtn">Contact Us</a> -->
          </div>
        </div>
        <div class="col-lg-4 col-md-12 col-sm-12 col-xs-12">
          <div id="accordion" class="panelAccordion">
            <div class="card">
              <div class="card-header" id="headingOne">
                <p class="mb-0 active">
                  <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                    our mission
                  </button>
                  <i class="fa fa-angle-down" aria-hidden="true"></i>
                </p>
              </div>

              <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                <div class="card-body">
                  <p class="paraText">Our mission is to help and enable the students who are preparing for their NEET exam to add value to their prepartion.  We would like to support and provide them the guidance in terms of providing detailed analysis and report for their mock exam and their preparedness.</p>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section>
  <div class="ctaSecond">
    <div class="container">
      <div class="flexBoxCta">
        <p class="ctaText black"> Do you want to participate in <span class="textMain">All India NEET Mock Test </span>being conducted by us, then please register. <span class="textMain">It is absolutely FREE!!!</span></p>
        <!-- <p class="midText ctaPara">Lorem ipsum dolor sit amet consectetur</p> -->
        <a class="hvr-ripple-out primeBg caps btn radiusBtn register_for_scheduled_exam_text">Sign Up</a>
      </div>
    </div>
  </div>
</section>
<!-- CTA Section -->

<!-- Services Section -->
<section class="animated wow fadeInUp" id="servicesSection">
  <div class="offsetLg">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
              <h2 class="mainHeading">Services</h2>
                <div class="hrTag">
                  <img class="hrTag_pencil" src="img/hr.png" alt="hr">
                </div>
            </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <div class="card-deck">
            <div class="owl-carousel">
              <div class="col-md-12 offset30">
                  <div class="cardzz hvr-sweep-to-right">
                    <img class="card-img-top" src="img/fourth.jpg" alt="Card image cap">
                    <div class="card-body">
                    <p class="card-title">NEET Previous Year Exam Questions 
                      <br>
                    <span class="cardDisc"><button type="button" class="btn btn-success">Buy</button></span></p>
										<p class="card-text">There are 12 Previous years questions are available for the students to performand practice the questions and get the score out of it.	There are two ways that the students 
											<div class="card-text" id="card4_more_data" style="display:none">
										 can write the mock exam or otherwise they can read all the questions and answers in Study mode. There are few free questions sets available and rest of themare paid.  Please register and know with free exams and buy more to get practicethe exams.  As much as practice the mock exams and previous years questions will givean more confident and possibility to get thorugh the NEET exam.
												<p id="card4_more_data_less_link" class="card-text-new"><a    class="hvr-forward text-muted">Read Less <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></p>
											</div>
										</p>
										<p class="card-text-new "><a  id="card4_more_data_more_link" class="hvr-forward text-muted">Read more <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></p>
                    </div>
                  </div>
               </div>
                <div class="col-md-12 offset30">
                  <div class="cardzz hvr-sweep-to-right">
                    <img class="card-img-top" src="img/fifth.jpg" alt="Card image cap">
                    <div class="card-body">
                                    <p class="card-title">NEET Mock Tests<br>
                    <span class="cardDisc"><button type="button" class="btn btn-success">Buy</button></span></p>
                                    <p class="card-text">There are many NEET mock exams available for the students to practice and test their knowledge.  We are providing 5 free full NEET exams and the rest of them are paid 
																			<div class="card-text" id="card5_more_data" style="display:none">
                                      one.	The students can buy the packs whenever they want to test their knowledge and they will get the detailed results of their mock exams.  That will help them to know which subject they are strong and in which subjects they have to improve.  Registered studentscan see their historical test results to know their travel towards the NEET final exam.
																				<p id="card5_more_data_less_link" class="card-text-new"><a    class="hvr-forward text-muted">Read Less <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></p>
																			</div>
																		</p>
                      							<p class="card-text-new "><a  id="card5_more_data_more_link" class="hvr-forward text-muted">Read more <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></p>
                    </div>
                  </div>
               </div>
                <div class="col-md-12 offset30">
                  <div class="cardzz hvr-sweep-to-right">
                    <img class="card-img-top" src="img/sixth.jpg" alt="Card image cap">
                    <div class="card-body">
										<p class="card-title">All India NEET Mock Test<br>
                    <span class="cardDisc"> <button type="button" class="btn btn-success">Sign Up Now</button></span></p>
										<p class="card-text">Any student can register for All India NEET Mock Test conducting by us.  It is absolutely FREE for all.  This is a scheduled exam happens   every second Sunday or as per
											<div class="card-text" id="card6_more_data" style="display:none">
										 the Noticeboard displayed on top of this website.  Students across India can register and participatethe Mock Test and get their results as per our internal All India NEET Mock Test.   This will help the students to understand their knowledge and progress towards their NEET exampreparation.  This All India NEET Mock Test is just to test your knowledge and readinessand this is not an official or ranking to do with any academic activities.	
												<p id="card6_more_data_less_link" class="card-text-new"><a    class="hvr-forward text-muted">Read Less <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></p>
											</div>
										</p>
										<p class="card-text-new "><a  id="card6_more_data_more_link" class="hvr-forward text-muted">Read more <i class="fa fa-long-arrow-right" aria-hidden="true"></i></a></p>
									</div>
                  </div>
               </div>

                </div>
              </div>
            </div>
          </div>
      <div class="row">

      </div>
    </div>
  </div>
</section>
<!-- Services Section -->

<!-- CTA Section -->
  <section>
    <div class="ctaImg">
      <div class="container">
        <div class="row">
          <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
            <!-- <p class="lightText ctaText">We’re your appliance and <span class="textMain">home improvement</span> experts.</p> -->
            <p class="lightText midText">Hey students,  we are here to help you to test your knoweldge and provide a complete report about your preparation for NEET exam.   Our platform basically helps you to do a self measurements about your preparation for NEET exam and to gear up your studies to face the NEET exam as a mantra of   <br> "I FACE NEET EXAM".</p>
          </div>
          <div class="centered col-lg-3 col-md-3 col-sm-12 col-xs-12">
            <a class="lightText hvr-ripple-out primeBg caps offset30 radiusBtnOutline pull-right"><i class="fa fa-paper-plane-o"></i>Sign Up</a>
          </div>
        </div>
      </div>
    </div>
  </section>
<!-- CTA Section -->

<!-- FAQ Section -->

<section class="animated wow fadeInUp" id="faqSection">
  <div class="offsetLg">
    <div class="container">
      <div class="row">
        <div class="col-md-12 text-center">
              <h2 class="mainHeading">Frequently Asked Questions</h2>
                <div class="hrTag">
                  <img class="hrTag_pencil" src="img/hr.png" alt="hr">
                </div>
            </div>
      </div>
      <div class="row offset30">
        <div class="col-md-12">
              <div id="accordionFaq" class="faqAccordion">
              <!-- set dynamically faq question here -->
                <?php 
                   if(!empty($all_faq)){
                      foreach ($all_faq as $index => $question_set) : ?>
                        <div class="card">
                <!-- first faq question -->
                  <div class=" card-header" id="heading<?php echo $question_set['Faq']['id']?>">
                    <p class="mb-0 ">
                      <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse<?php echo $question_set['Faq']['id']?>" aria-expanded="false" aria-controls="collapseOne<?php echo $question_set['Faq']['id']?>">
                      <?php echo $question_set['Faq']['faq_ques'] ?>
                      </button>
                       <i class="fa fa-chevron-circle-right" aria-hidden="true"></i> 
                    </p>
                  </div>
                  <!-- first question end -->
                    <!-- first question answer -->
                  <div id="collapse<?php echo $question_set['Faq']['id']?>" class="collapse " aria-labelledby="heading<?php echo $question_set['Faq']['id']?>" data-parent="#accordionFaq" style="">
                    <div class="card-body">
                      <p class="paraText"><?php echo $question_set['Faq']['faq_ans'] ?> </p>
                    </div>
                  </div>
                  <!-- first question answer end -->
                </div>
                <?php endforeach; } ?>
              <!-- end dynamically question here -->
                
                <!-- <div class="card">
                
                  <div class=" card-header" id="headingOne">
                    <p class="mb-0 ">
                      <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne1" aria-expanded="false" aria-controls="collapseOne1">
                      Who will be conducting NEET (UG)?
                      </button>
                      <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                    </p>
                  </div>
                  <div id="collapseOne1" class="collapse " aria-labelledby="headingOne" data-parent="#accordionFaq" style="">
                    <div class="card-body">
                      <p class="paraText">The Central Board of Secondary Education (CBSE) will be conducting the National Eligibility Cum Entrance Test for admission to MBBS/BDS Courses in the session 2013-14. </p>
                    </div>
                  </div>
                  
                </div>

                <div class="card">
                  <div class="card-header" id="headingTwo2">
                    <p class="mb-0">
                      <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo2" aria-expanded="false" aria-controls="collapseTwo2">
                      Whether the exam. will be offline or online?
                      </button>
                      <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                    </p>
                  </div>
                  <div id="collapseTwo2" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionFaq">
                    <div class="card-body">
                      <p class="paraText">NEET (UG)-2013 will be an offline pen and paper test.</p>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingThree3">
                    <p class="mb-0">
                      <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree3" aria-expanded="false" aria-controls="collapseThree3">
                      What will be the syllabus of NEET (UG)?
                      </button>
                     <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                    </p>
                  </div>
                  <div id="collapseThree3" class="collapse" aria-labelledby="headingThree" data-parent="#accordionFaq">
                    <div class="card-body">
                       <p class="paraText">The Question Papers for NEET shall be based on a common syllabus notified by the Medical council of India which is available on website  <a href="https://www.mciindia.org">www.mciindia.org .</a></p>
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingThree4">
                    <p class="mb-0">
                      <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree4" aria-expanded="false" aria-controls="collapseThree4">
                      What is the official website of the Board for National Eligibility Cum Entrance Test (NEET)?
                      </button>
                     <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>
                    </p>
                  </div>
                  <div id="collapseThree4" class="collapse" aria-labelledby="headingThree" data-parent="#accordionFaq">
                    <div class="card-body">
                       <p class="paraText">The official website of the Board for National Eligibility Cum Entrance Test (NEET) is <a href="https://www.cbseneet.nic.in">www.cbseneet.nic.in</a> . All the examination related information will be available on this website.</p>
                    </div>
                  </div>
                </div> -->



           </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- FAQ Section -->

<!-- Cta Section -->
<div class="ctaRating">
  <div class="container">
    <div class="row">
      <div class="col-md-3">
        <div class="iconList">
          <i class="icon-customer" aria-hidden="true"></i>
          <p class="amount count">1200</p>
          <p class="tittleAmount">Unique questions</p>
        </div>
      </div>
      <div class="col-md-3">
        <div class="iconList">
          <i class=" icon-trophy" aria-hidden="true"></i>
          <p class="amount count">500</p>
          <p class="tittleAmount">Happy Students</p>
        </div>
      </div>
      <div class="col-md-3">
        <div class="iconList">
          <i class="icon-mechanic" aria-hidden="true"></i>
          <p class="amount count">152</p>
          <p class="tittleAmount">Full Mock tests</p>
        </div>
      </div>
      <div class="col-md-3">
        <div class="iconList">
          <i class="icon-antenna" aria-hidden="true"></i>
          <p class="amount count">765</p>
          <p class="tittleAmount">All India NEET Mock Test completed</p>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Cta Section -->

<!-- Testmonial Section -->
<?php 
        if(!empty($all_faq)){ ?>
          <section class="animated wow fadeInUp" id="testmonialSection">
          <div class="offsetLg">
            <div class="container">
                    <div class="row">
                            <div class="col-md-12 text-center">
                              <h2 class="mainHeading">Testimonials</h2>
                                <div class="hrTag">
                                  <img  class="hrTag_pencil" src="img/hr.png" alt="hr">
                                </div>
                            </div>
                    </div>
                  <div id="demo" class="carousel slide" data-ride="carousel">
                      <div class="carousel-inner">
                        <?php foreach ($testimonial_datas as $index => $testimonial_data) : ?>
                          <div class="carousel-item <?= $testimonial_data['status'] ?> ">
                            <div class="row">
                              <?php foreach($testimonial_data['testimonials'] as $tesIndex => $testimonial) : ?>
                                <div class="col-md-6 col-sm-12 col-xs-12 offset30">
                                  <div class="hvr-shadow-radial testUser" style="width:100%;">
                                      <p class="common-title"><?= $testimonial['Testimonial']['name'] ?></p>
                                      <p class="common-text"><?= $testimonial['Testimonial']['comment'] ?></p>
                                      <?php for($starCount = 1; $starCount <= 5; $starCount++) { ?>
                                          <?php if($starCount <= $testimonial['Testimonial']['rating']) { ?>
                                            <i class="activeStar fa fa-star-o"></i>
                                          <?php } else { ?>
                                            <i class="fa fa-star-o"></i>
                                          <?php }?>
                                      <?php }?>
                                  </div>
                                </div> 
                              <?php endforeach;  ?> 
                            </div>
                          </div>
                          <?php endforeach;  ?> 
                        <a class="carousel-control-prev" href="#demo" data-slide="prev">
                          <span class="carousel-control-prev-icon"></span>
                        </a>
                        <a class="carousel-control-next" href="#demo" data-slide="next">
                          <span class="carousel-control-next-icon"></span>
                        </a>
                    
                    
                    
                      </div>
                  </div>
            </div>
          </div>
        </section>     
     <?php  } ?>
<!-- Testmonial Section -->

<!-- CTA Section -->
<section>
  <div class="offsetXl ctaSection">
    <div class="container">
      <div class="row">
      <div class="ctaContent">
        <div class=" col-md-9">
        <p class="ctaText">Looking for a <span class="textMain">previous years NEET question set with answers</span> and list of NEET Mock Test series to know your readiness<span class="textMain"> then please register here....</span> </p>
        </div>
        <div class=" col-md-3 pull-right">
          <a class="hvr-ripple-out primeBg caps btn radiusBtn"><i class="fa fa-paper-plane-o"></i>Sign Up</a>
        </div>
      </div>
    </div>
    </div>
  </div>
</section>


<section id="contactSection">
  <div class="contact" >
    <div class="container">
        <div class="contactPage aboutSection offsetLg">
        <div class="row">
        <div class="col-md-12 text-center">
              <h2 class="mainHeading">Contact Us</h2>
                <div class="hrTag">
                  <img class="hrTag_pencil" src="img/hr.png" alt="hr">
                </div>
            </div>
      </div>
          <div class="row offset30">
            <div class="col-md-12">
              <form>
                <div class="form-row">
                  <div class="form-group col-md-6">
                    <label for="inputEmail4">y o u r  &nbsp;  n a m e</label>
                    <input type="text" class="form-control" id="inputEmail4" >
                  </div>
                  <div class="form-group col-md-6">
                    <label for="inputPassword4">e m a i l  &nbsp; a d d r e s s</label>
                    <input type="email" class="form-control" id="inputPassword4" >
                  </div>
                </div>
                 <div class="form-row ">
                  <div class="form-group col-md-6">
                    <label for="inputEmail4">p h o n e</label>
                    <input type="text" class="form-control" id="inputEmail4" >
                  </div>
                  <div class="form-group col-md-6">
                    <label for="inputPassword4">c o m m e n t s</label>
                    <input type="text" class="form-control" id="inputPassword4" >
                  </div>
                </div>

                <div class="col-md-12 text-center" style="margin-top:10px">
                  <button type="submit" class="hvr-ripple-out primeBg caps btn radiusBtn textCenter">submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>
    </div>
  </div>
</section>
<script>
$("i").hover(function(){
    $(".btn_link").css("color", "black");
    }, function(){
    $(".btn-link").css("color", "black");
});
</script>

<script>
    var cd = new Countdown({
        cont: document.querySelector('.timer'),
        endDate: <?php echo $exam_time_stamp ?>,
                 
        outputTranslation: {
            year: 'Years',
            week: 'Weeks',
            day: 'Days',
            hour: 'Hours',
            minute: 'Minutes',
            second: 'Seconds',
        },
        endCallback: null,
        outputFormat: 'day|hour|minute|second',
    });
    cd.start('<?php echo $exam_time ?>');
</script>
<?php echo $this->element('footer') ?>


